$(function() {
    $('#enterprise_name').change(function() {
        $('.empresa_nome').html($('#enterprise_name').val());
    });

    $('#advertising_title').change(function() {
        $('.anuncio_titulo').html($('#advertising_title').val());
    });

    $('#category').change(function() {
        $.getJSON('/ajax/getservicelistbycategoryid/', {categoryid: $(this).val(), limit: false}, function(j) {
            var data_source = '';
            if (j.length > 0) {
                for (var i = 0; i < j.length; i++) {
                    data_source += j[i].name + "";
                    if (i !== (j.length - 1)) {
                        data_source += ',';
                    } else {
                        data_source += ',Outros';
                    }
                }
            } else {
                data_source = null;
            }
            console.log(data_source);
            //data_source += ']';
            //$("#ms1").attr('data',data_source);

            $('#ms1').magicSuggest({
                noSuggestionText: 'Sem sugestões',
                maxSelection: 1,
                maxSuggestions: 4,
                name: 'services'

            }).setData(data_source).empty();
        });
        //$('#category').attr('readonly', true); 
        $('#ms1').magicSuggest({
            noSuggestionText: 'Sem sugestões',
            maxSelection: 1,
            maxSuggestions: 4,
            name: 'services'

        }).clear(true);

        $('.empresa_categoria').html($('#category option:selected').text());
        $('#category_name_text').attr('value', $('#category option:selected').text())
    });

    $('#advertising_content').change(function() {
        $('.anuncio_conteudo').html($('#advertising_content').val());
    });

    $('#address').change(function() {
        $('.endereco').html($('#address').val());
    });

    $('#address_number').change(function() {
        $('.endereco_numero').html($('#address_number').val());
    });

    $('#address_city').change(function() {
        $('.endereco_cidade').html($('#address_city option:selected').text());
        $('#city_name_text').attr('value', $('#address_city option:selected').text())
    });

    $('#address_district').change(function() {
        $('.endereco_bairro').html($('#address_district').val());
    });

    $('#address_state').change(function() {
        if ($(this).val()) {
            $('#address_city').hide();
            $('.carregando').show();
            $.getJSON('/ajax/getcitylist', {state: $(this).val()}, function(j) {
                var options = '<option>Cidade</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].idcity + '">' + j[i].city_name + '</option>';
                }
                $('#address_city').html(options).show();
                $('.carregando').hide();
            });
        } else {
            $('#address_city').html('<option value="0">-- Escolha um estado --</option>');
        }
        $('.endereco_estado').html($('#address_state option:selected').text());
        $('#state_name_text').attr('value', $('#address_state option:selected').text())
    });
    
    $('.contato_tipo1').html($('#contact_type1 option:selected').text());
    $('.contato_tipo2').html($('#contact_type2 option:selected').text());
    $('.contato_tipo3').html($('#contact_type3 option:selected').text());
    $('.contato_tipo4').html($('#contact_type4 option:selected').text());
    $('.contato_tipo5').html($('#contact_type5 option:selected').text());
    
    $('#contact_type1').change(function(){
        $('.contato_tipo1').html($('#contact_type1 option:selected').text());
    });

    $('#contact_type2').change(function(){
        $('.contato_tipo2').html($('#contact_type2 option:selected').text());
    });

    $('#contact_type3').change(function(){
        $('.contato_tipo3').html($('#contact_type3 option:selected').text());
    });

    $('#contact_type4').change(function(){
        $('.contato_tipo4').html($('#contact_type4 option:selected').text());
    });

    $('#contact_type5').change(function(){
        $('.contato_tipo5').html($('#contact_type5 option:selected').text());
    });

    $('#value1').change(function() {
        $('.contato1').html($('#value1').val());
    });

    $('#value2').change(function() {
        $('.contato2').html($('#value2').val());
    });

    $('#value3').change(function() {
        $('.contato3').html($('#value3').val());
    });

    $('#value3').change(function() {
        $('.contato3').html($('#value3').val());
    });

    $('#value4').change(function() {
        $('.contato4').html($('#value4').val());
    });

    $('#value5').change(function() {
        $('.contato5').html($('#value5').val());
    });

});

$(document).ready(function() {
    $('#searchTerm').change(function() {
        $('#main_form_search').attr("action", $('#main_form_search').attr("action") 
        + encodeURI($('#searchTerm').val()));
    });
});
    
$(document).ready(function() {
    // Smart Wizard     	
    $('#wizard').smartWizard({
        transitionEffect: 'slideleft',
        onLeaveStep: leaveAStepCallback,
        onFinish: onFinishCallback,
        enableFinishButton: true,
        labelNext: 'Pr&oacute;ximo',
        labelPrevious: 'Voltar',
        labelFinish: 'Finalizar'
    });
    function leaveAStepCallback(obj) {
        var step_num = obj.attr('rel');
        return validateSteps(step_num);
    }

    function onFinishCallback() {
        //if (validateAllSteps()) {
            $('#formModalAnuncio').submit();
        //}
    }

});

function validateAllSteps() {
    var isStepValid = true;

    if (validateStep1() === false) {
        isStepValid = false;
        $('#wizard').smartWizard('setError', {stepnum: 1, iserror: true});
    } else {
        $('#wizard').smartWizard('setError', {stepnum: 1, iserror: false});
    }

    if (validateStep3() === false) {
        isStepValid = false;
        $('#wizard').smartWizard('setError', {stepnum: 3, iserror: true});
    } else {
        $('#wizard').smartWizard('setError', {stepnum: 3, iserror: false});
    }

    if (!isStepValid) {
        $('#wizard').smartWizard('showMessage', 'Por favor corrija os passos e então prossiga.');
    }

    return isStepValid;
}


function validateSteps(step) {
    var isStepValid = true;
    // validate step 1
    if (step === 1) {
        if (validateStep1() === false) {
            isStepValid = false;
            $('#wizard').smartWizard('showMessage', 'Por favor corrija o passo' + step + ' e então prossiga.');
            $('#wizard').smartWizard('setError', {stepnum: step, iserror: true});
        } else {
            $('#wizard').smartWizard('hideMessage');
            $('#wizard').smartWizard('setError', {stepnum: step, iserror: false});
        }
    }

    // validate step3
    if (step === 3) {
        if (validateStep3() === false) {
            isStepValid = false;
            $('#wizard').smartWizard('showMessage', 'Por favor corrija o passo' + step + ' e então prossiga.');
            $('#wizard').smartWizard('setError', {stepnum: step, iserror: true});
        } else {
            $('#wizard').smartWizard('hideMessage');
            $('#wizard').smartWizard('setError', {stepnum: step, iserror: false});
        }
    }

    return isStepValid;
}

function validateStep1() {
    var isValid = true;

    // Validate enterprise name
    var un = $('#enterprise_name').val();
    if (!un && un.length <= 0) {
        isValid = false;
        $('#msg_enterprise_name').html('Please fill username').show();
    } else {
        $('#msg_enterprise_name').html('').hide();
    }

    // Validate category
    var un = $('#category').val();
    if (!un && un.length <= 0) {
        isValid = false;
        $('#msg_category').html('Please fill username').show();
    } else {
        $('#msg_category').html('').hide();
    }

    return isValid;
}

function validateStep3() {
    var isValid = true;

    // Validate category
    var un = $('#address').val();
    if (!un && un.length <= 0) {
        isValid = false;
        $('#msg_address').html('Please fill username').show();
    } else {
        $('#msg_address').html('').hide();
    }

    return isValid;
}



$(".anuncioTelefone").hide();
$(".btn-contacts").click(function() {
    var enterpriseId = $(this).parent().attr('data-enterprise-id');
    var parent = $(this).parent();
    var items = 0;
    $.getJSON('/ajax/getcontactinfo/enterpriseid/' + enterpriseId, function(data) {
        contactList = '';
        $.each(data, function(key, val) {
            if(val['type_name'] == 'Telefone Fixo'){ icon = 'icon-phone'}
            if(val['type_name'] == 'Email'){ icon = 'icon-envelope'; val['value'] = '<a href="mailto:' + val['value'] + '">' + val['value'] + '</a>'}
            if(val['type_name'] == 'Nextel'){ icon = 'icon-mobile-phone'}
            if(val['type_name'] == 'Telefone Celular'){ icon = 'icon-mobile-phone'}
            if(val['type_name'] == 'Website'){ icon = 'icon-globe'; val['value'] = '<a href="http://' + val['value'] + '">' + val['value'] + '</a>'}
            if(val['type_name'] == 'Facebook'){ icon = 'icon-facebook'; val['value'] = '<a href="http://www.facebook.com/' + val['value'] + '">' + val['value'] + '</a>'}
            if(val['type_name'] == 'Twitter'){ icon = 'icon-twitter'; val['value'] = '<a href="http://www.twitter.com/' + val['value'] + '">@' + val['value'] + '</a>'}
            if(val['type_name'] == 'Youtube'){ icon = 'icon-youtube'}
            
            contactList += '<strong><i class="'+icon+'"></i> ' + val['type_name'] + '</strong>:' + ' ' + val['value'] + '<br />';
            items += 1;
        });
        if (items > 0) {
            $("#anuncioTelefone_" + enterpriseId).html(contactList);
        }
        else {
            $("#anuncioTelefone_" + enterpriseId).html('<strong class="error"><i class="icon-remove"></i> Sem informações de contato cadastrada.</strong>');
        }
    });
    $("#anuncioTelefone_" + enterpriseId).toggle();
    $(this).parent().find(".btn-contacts").hide();
});

$.getJSON('/ajax/getcategorylist', function(j) {
    var options = '<option>Selecione a categoria de atividade comercial</option>';
    for (var i = 0; i < j.length; i++) {
        options += '<option value="' + j[i].idcategory + '">' + j[i].category_name + '</option>';
    }
    $('#category').html(options).show();
});

$("button[data-toggle=popover]").popover({
    placement: 'left',
    title: 'Ajuda'
});

$(function() {
   
    $(".anuncioOpcoes").hide();
    
    $(".guiaResultadoBuscaAnuncio")
        .mouseover( function(){
            $(this).find(".anuncioOpcoes").show();
        })
        .mouseout( function(){
            $(this).find(".anuncioOpcoes").hide();
    }); 
    
    
    
});