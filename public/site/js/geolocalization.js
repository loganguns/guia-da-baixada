$(function(){
    getGeolocalization();
});
function success(position){
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var latlng = lat + ',' + lng;
    
    //if(latlng !== $.cookie("latlng")){
    //$.cookie("latlng", latlng);
        
        $.getJSON('/ajax/setgeolocationinfo/latlng/' + latlng, function(data){
            //console.log(data);
            if(data.status == 'changed'){
                location.reload();
            }
        });
    //}
}
function error(msg){
    var s = document.querySelector('#geo_status');
    s.innerHTML = typeof msg === 'string' ? msg : "<strong>Atenção!</strong> Você não autorizou pegar as informações de sua localidade. Porfavor atualize a página e autorize a coleta de informações sobre sua localidade.";
    s.className = 'alert alert-error';
}
function getGeolocalization(){
    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(success, error);
    }
    else{
        error('<strong>Atenção!</strong> Geolocalização não suportada pelo seu navegador. Porfavor atualize seu navegador.');
    }
}