if($.cookie("__lat") == null && $.cookie("__lon") == null ){
    getGeolocalization();
}
function success(position){
    $.cookie("__lat", position.coords.latitude);
    $.cookie("__lon", position.coords.longitude);
    
    $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+'&sensor=false'
            , function(data) {
        console.log(data);
        $.cookie("__city", data['results'][1]['address_components'][0]['long_name']);
        $.cookie("__state", data['results'][1]['address_components'][1]['long_name']);
        location.reload();
    });
}
function error(msg){
    var s = document.querySelector('#geo_status');
    s.innerHTML = typeof msg == 'string' ? msg : "failed";
    s.className = 'alert alert-error';
}
function getGeolocalization(){
    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(success, error);
    }
    else{
        error('Geolocalização não suportada pelo seu navegador.');
    }
}