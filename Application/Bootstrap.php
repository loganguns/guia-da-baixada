<?php
session_name(md5('seg'.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']));
date_default_timezone_set('America/Sao_Paulo');
//error_reporting(0);
error_reporting(E_ALL | E_STRICT);

set_include_path('.'
        . PATH_SEPARATOR . '../Libs/'
        . PATH_SEPARATOR . './'
        . PATH_SEPARATOR . './Models/'
        . PATH_SEPARATOR . './Controllers/'
        . PATH_SEPARATOR . './Controllers/Plugin/'
);

include "../Libs/Zend/Loader.php";

//require_once "../libs/bugsnag.php";
//Bugsnag::register("2f59373542cd0b107f887b2ef620fe8b");
//set_error_handler("Bugsnag::errorHandler");
//set_exception_handler("Bugsnag::exceptionHandler");

Zend_Loader::loadClass('Zend_Controller_Front');
Zend_Loader::loadClass('Zend_Registry');
Zend_Loader::loadClass('Zend_View');
Zend_Loader::loadClass('Zend_Config_Ini');
Zend_Loader::loadClass('Zend_Db');
Zend_Loader::loadClass('Zend_Db_Table');
Zend_Loader::loadClass('Zend_Debug');
Zend_Loader::loadClass('Zend_Auth');
Zend_Loader::loadClass('Zend_Log');
Zend_Loader::loadClass('Zend_Log_Writer_Stream');
//Zend_Loader::loadClass('Zend_Controller_Plugin_ErorHandler');
Zend_Loader::loadClass('Zend_Session_Namespace');
//Zend_Loader::loadClass('ErrorHandler');
Zend_Loader::loadClass('HasIdentity');
Zend_Loader::loadClass('GeoLocation');
Zend_Loader::loadClass('Teste');
Zend_Loader::loadClass('Zend_Controller_Router_Route');
//Zend_Loader::loadClass('Acl_Setup');
//Zend_Loader::loadClass('HasAuth');
//set_error_handler("ErrorHandler::show");
//set_exception_handler("ErrorHandler::show");

$siteconfig = new Zend_Config_Ini('./Configs/config.ini', 'site');
$title = $siteconfig->config->title;
$title_dashboard = $siteconfig->config->title_dashboard;
$name = $siteconfig->config->name;

// DB setup
/**
 * Função faz a conexão com o banco de dados e registra a variável $db para
 * que ela esteja disponível em toda a aplicação.
 */
class _initConnection {

    function __construct() {

        try {
            $dbconfig = new Zend_Config_Ini('./Configs/config.ini', 'dbsettings');
            $db = Zend_Db::factory($dbconfig->db->adapter, $dbconfig->db->config->toArray());
            $db->getConnection();

            Zend_Db_Table::setDefaultAdapter($db);
            Zend_Registry::set('DB', $db);
            $dataSql = "SET NAMES 'utf8';";
            $dataSql .= "SET character_set_connection=utf8;";
            $dataSql .= "SET character_set_client=utf8;";
            $dataSql .= "SET character_set_results=utf8;";
            $db->getConnection()->query($dataSql);
        } catch (Zend_Exception $exc) {

            echo $exc->getTraceAsString();
            //echo utf8_decode("Estamos sem conexão ao banco de dados neste momento. Tente mais tarde por favor.");

            exit;
        }
    }

}

new _initConnection();

$fc = Zend_Controller_Front::getInstance();
$router = $fc->getRouter();

$about_route = new Zend_Controller_Router_Route(
"sobre",
array(
    "controller" => "portal",
    "action" => "sobre"
    )
);
$router->addRoute("about", $about_route);

$representatives_route = new Zend_Controller_Router_Route(
"representantes",
array(
    "controller" => "portal",
    "action" => "representantes"
    )
);
$router->addRoute("representatives", $representatives_route);

$partners_route = new Zend_Controller_Router_Route(
"parceiros",
array(
    "controller" => "portal",
    "action" => "parceiros"
    )
);
$router->addRoute("partners", $partners_route);

$search_route = new Zend_Controller_Router_Route(
"/:searchTerm",
array(
    "controller" => "portal",
    "action" => "search"
    )
);
$router->addRoute("search", $search_route);

$client_route = new Zend_Controller_Router_Route(
"/:searchTerm/:clientName",
array(
    "controller" => "portal",
    "action" => "clientPage"
    )
);
//$router->addRoute("client", $client_route);

$characteres = array(
    'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
    'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
    'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
    'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
    'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
    'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
    'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f', ' '=> '_'
);

//echo strtolower(strtr('H1 Internet Serviços de Informática', $characteres));

/*$state = array(
    'acre',
    'alagoas',
    'amapa',
    'amazonas',
    'bahia',
    'ceara',
    'distrito-federal',
    'espirito-santo',
    'goias',
    'maranhao',
    'mato-grosso',
    'mato-grosso-do-sul',
    'minas-gerais',
    'para',
    'paraiba',
    'parana',
    'pernambuco',
    'piaui',
    'rio-de-janeiro',
    'rio-grande-do-norte',
    'rio-grande-do-sul',
    'rondonia',
    'roraima',
    'santa-catarina',
    'sao paulo',
    'sergipe',
    'tocantins',
    'todos-estados'
);
foreach($state as $curr){
    $rota_provisoria1 = new Zend_Controller_Router_Route(
    $curr.'/:city/:searchTerm',
    array(
        "controller" => "portal",
        "action" => "search"
        )
    );
    $router->addRoute($curr.'1', $rota_provisoria1);
    
    $rota_provisoria2 = new Zend_Controller_Router_Route(
    $curr.'/:city',
    array(
        "controller" => "portal",
        "action" => "search"
        )
    );
    $router->addRoute($curr.'2', $rota_provisoria2);
    
    $rota_provisoria3 = new Zend_Controller_Router_Route(
    $curr,
    array(
        "controller" => "portal",
        "action" => "search"
        )
    );
    $router->addRoute($curr.'3', $rota_provisoria3);
}
*/

  $dashboard_route = new Zend_Controller_Router_Route(
  "painel",
  array(
  "controller" => "dashboard",
  "action" => "index"
  )
  );
  $category_route = new Zend_Controller_Router_Route(
  "painel/categoria",
  array(
  "controller" => "dashboard",
  "action" => "category"
  )
  );$category_create_route = new Zend_Controller_Router_Route(
  "painel/categoria/acao/novo",
  array(
  "controller" => "dashboard",
  "action" => "category"
  )
  );
  $router->addRoute("categoria", $category_route);
  $router->addRoute("dashboard", $dashboard_route);
  $router->addRoute("categoria_novo", $category_create_route);
 
//new Acl_Setup();
require_once './Views/ZendViewSmarty.php';
$view = new Zend_View_Smarty('./Views/templates/');
$view->assign('title', $title);
$view->assign('title_dashboard', $title_dashboard);
Zend_Registry::set('smarty', $view);
$frontController = Zend_Controller_Front::getInstance();
$frontController->throwExceptions(false);
$frontController->registerPlugin(new HasIdentity());
//$frontController->registerPlugin(new GeoLocation());
//$frontController->registerPlugin(new Teste());
//$frontController->returnResponse(true);
$frontController->setControllerDirectory('./Controllers/');
$frontController->setParam('noViewRenderer', true);
$frontController->setDefaultControllerName('Portal');
$frontController->dispatch();