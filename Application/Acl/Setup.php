<?php

/**
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

Zend_Loader::loadClass('Zend_Acl');

class Acl_Setup{
    private $_acl;
    
    public function __construct()
    {
        $this->_acl = new Zend_Acl();
        $this->_initialize();
    }

    protected function _initialize()
    {
        $this->_setupRoles();
        $this->_setupResources();
        $this->_setupPrivileges();
        $this->_saveAcl();
    }

    protected function _setupRoles()
    {
        $this->_acl->addRole( new Zend_Acl_Role('guest') );
        $this->_acl->addRole( new Zend_Acl_Role('user'), 'guest' );
        $this->_acl->addRole( new Zend_Acl_Role('admin'), 'user' );
    }

    protected function _setupResources()
    {
        $this->_acl->addResource( new Zend_Acl_Resource('painel') );
        $this->_acl->addResource( new Zend_Acl_Resource('auth') );
        $this->_acl->addResource( new Zend_Acl_Resource('dashboard') );
        $this->_acl->addResource( new Zend_Acl_Resource('error') );
        $this->_acl->addResource( new Zend_Acl_Resource('index') );
    }

    protected function _setupPrivileges()
    {
        $this->_acl->allow( 'guest', 'auth', array('index', 'login') )
                   ->allow( 'guest', 'error', array('error', 'forbidden') )
                   ->allow( 'guest', 'index', array('index'));
        
        $this->_acl->allow( 'user', 'painel', array('index') )
                   ->allow( 'user', 'auth', 'logout' );
        
        //$this->_acl->allow( 'admin', 'dashboard', array('index', 'adicionar') );
    }

    protected function _saveAcl()
    {
        $registry = Zend_Registry::getInstance();
        $registry->set('acl', $this->_acl);
    }
}