<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
<head>
	<title>{$title_dashboard}</title>
	
	<!-- Meta -->
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	
	<!-- Bootstrap -->
	<link href="/public/dashboard/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/public/dashboard/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	
	<!-- Bootstrap Extended -->
	<link href="/public/dashboard/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet" />
	<link href="/public/dashboard/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="/public/dashboard/bootstrap/extend/bootstrap-wysihtml5/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet" />
	
	<!-- JQueryUI v1.9.2 -->
	<link rel="stylesheet" href="/public/dashboard/theme/scripts/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css" />
	
	<!-- Glyphicons -->
	<link rel="stylesheet" href="/public/dashboard/theme/css/glyphicons.css" />
	
	<!-- Bootstrap Extended -->
	<link rel="stylesheet" href="/public/dashboard/bootstrap/extend/bootstrap-select/bootstrap-select.css" />
	<link rel="stylesheet" href="/public/dashboard/bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	
	<!-- Uniform -->
	<link rel="stylesheet" media="screen" href="/public/dashboard/theme/scripts/pixelmatrix-uniform/css/uniform.default.css" />

	<!-- JQuery v1.8.2 -->
	<!--script src="/public/dashboard/theme/scripts/jquery-1.8.2.min.js"></script-->
        
        <!-- JQuery v1.9.1-->
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	
	<!-- Modernizr -->
	<script src="/public/dashboard/theme/scripts/modernizr.custom.76094.js"></script>
	
	<!-- MiniColors -->
	<link rel="stylesheet" media="screen" href="/public/dashboard/theme/scripts/jquery-miniColors/jquery.miniColors.css" />
	
	<!-- Theme -->
	<link rel="stylesheet" href="/public/dashboard/theme/css/style.min.css?1359188708" />
	
	<!-- LESS 2 CSS -->
	<script src="/public/dashboard/theme/scripts/less-1.3.3.min.js"></script>
        
        <!-- Token Input -->
        <script type="text/javascript" src="/public/dashboard/js/jquery.tokeninput.js"></script>
	
        <!-- Select File-->
        <script type="text/javascript" src="/public/dashboard/bootstrap/js/bootstrap.file-input.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
	<!-- Start Content -->
	<div class="container-fluid left-menu">
		
		<div class="navbar main">
                    <div class="innerpx">
                        <button type="button" class="btn btn-navbar hidden-desktop hidden-tablet">
                                <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                        </button>
                        <div class="positionWrapper">
                            <span class="line"></span>
                            <div class="profile">
                                <img src="http://www.placehold.it/38x38/232323" class="avatar" alt="Profile" />
                                <span class="info hidden-phone">
                                        <strong>{$userName}</strong>
                                        <em>H1 Internet</em>
                                </span>
                            </div>
                            <ul class="topnav hidden-phone">
                                <li>
                                    <a href="/painel/logoff" class="logout glyphicons lock"><i></i><span>Sair</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
		</div>
	
		<div class="row-fluid rrow main">
			<div class="span12 main col" role="main">
				<div class="row-fluid rrow">
					<div class="span2 col main-left hide hidden-phone menu-large">
						<div class="rrow scroll-y-left">
							<div class="iScrollWrapper">
								<ul>
									<li class="glyphicons home{if $action == 'index'} currentScroll active{/if}"><a href="/painel/"><i></i><span>Resumo</span></a></li>
                                                                        <li class="glyphicons list{if $action == 'categoria'} currentScroll active{/if}"><a href="/painel/categoria/"><i></i><span>Categorias</span></a></li>
                                                                        <li class="glyphicons cogwheel{if $action == 'servico'} currentScroll active{/if}"><a href="/painel/servico/"><i></i><span>Serviços</span></a></li>
                                                                        <li class="glyphicons cargo{if $action == 'empresa'} currentScroll active{/if}"><a href="/painel/empresa/"><i></i><span>Empresas</span></a></li>
                                                                        <li class="glyphicons notes{if $action == 'anuncio'} currentScroll active{/if}"><a href="/painel/anuncio/"><i></i><span>Anúncios</span></a></li>
                                                                        <li class="glyphicons user{if $action == 'usuarios'} currentScroll active{/if}"><a href="/painel/usuarios/"><i></i><span>Usuários</span></a></li>
								</ul>
							</div>
							<span class="navarrow hide">
								<span class="glyphicons circle_arrow_down"><i></i></span>
							</span>
						</div>
					</div>
					<div class="span10 col main-right">
						<div class="rrow scroll-y" id="mainYScroller">
							<div class="inner topRight">{*<ul class="breadcrumb">
	<li><a href="index.html" class="glyphicons home"><i></i> Resumo</a></li>
	<li class="divider"></li>
	<li>H1 Internet - Soluções Web</li>
</ul>*}
<div class="separator"></div>
{*include "dashboard/common/erro.tpl"*}