{include "dashboard/common/header.tpl"}
<link rel="stylesheet" href="http://{$smarty.server.SERVER_NAME}/public/dashboard/styles/token-input-facebook.css" type="text/css" />

{literal}
<script>
$(function(){
    $('#address_state').change(function(){
        if( $(this).val() ) {
            $('#address_city').hide();
            $('.carregando').show();
            $.getJSON('http://{/literal}{$smarty.server.SERVER_NAME}{literal}/painel/ajax/acao/listcity',{state: $(this).val()}, function(j){
                var options = '<option value="">Selecione</option>';	
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].idcity + '">' + j[i].city_name + '</option>';
                }	
                $('#address_city').html(options).show();
                $('.carregando').hide();
            });
        } else {
            $('#address_city').html('<option value="0">-- Escolha um estado --</option>');
        }
    });
});

$(function () {
	function removeField() {
		$(".removeField").unbind("click");
		$(".removeField").bind("click", function () {
			i=0;
			$(".contacts fieldset.contact").each(function () {
				i++;
			});
			if (i>1) {
				$(this).parent().remove();
			}
		});
	}
	removeField();
	$(".addField").click(function () {
                newField = $(".contacts fieldset.contact:first").clone();
		newField.find("input").val("");
                newField.find("select").val("");
		newField.insertAfter(".contacts fieldset.contact:last");
		removeField();
	});
});

</script>
{/literal}
<script type="text/javascript">
    $(document).ready(function() {
        $("#service_arr").tokenInput("http://{$smarty.server.SERVER_NAME}/painel/ajax/acao/listService/", {
            preventDuplicates: true,
            theme: "facebook"
        });
    });
</script>
<h2 class="glyphicons cargo"><i></i> Empresas</h2>
<div class="separator"></div>
	
<form class="form-horizontal" style="padding-top: 10px; margin-bottom: 0;" id="validateSubmitForm" method="post" action="/painel/empresa/acao/novo" autocomplete="off">
	
	<div class="row-fluid">
            <div class="span8">
                    <div class="well" style="padding-bottom: 10px;">
                            <h4>Cadastro de Empresa</h4>
                            <hr class="separator" />
                            <div class="row-fluid">
                                <div class="span8">
                                        <div class="control-group">
                                            <label class="control-label" for="enterprise_name"> Nome da Empresa</label>
                                            <div class="controls"><input class="span12" id="enterprise_name" name="enterprise_name" type="text"/></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="enterprise_description"> Descrição</label>
                                            <div class="controls"><textarea class="span12" id="enterprise_description" name="enterprise_description"></textarea></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="service_arr"> Serviços</label>
                                            <div class="controls">
                                                <input type="text" id="service_arr" name="service_arr" class="span12"/>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <h4>Endereço</h4>
                            <hr class="separator" />
                            <div class="row-fluid">
                                <div class="span8">
                                    <div class="control-group">
                                        <label class="control-label" for="address_address"> Endereço</label>
                                        <div class="controls"><input class="span12" id="address_address" name="address_address" type="text"/></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address_number"> Número</label>
                                        <div class="controls"><input class="span12" id="address_number" name="address_number" type="text"/></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address_complement"> Complemento</label>
                                        <div class="controls"><input class="span12" id="address_complement" name="address_complement" type="text"/></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="type_address_idtype_address"> Tipo</label>
                                        <div class="controls">
                                            <select class="span12" id="type_address_idtype_address" name="type_address_idtype_address">
                                                <option value="1">Residêncial</option>
                                                <option value="2" selected="selected">Comercial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address_state">Estado</label>
                                        <div class="controls">
                                            <select class="span12" id="address_state" name="address_state">
                                                <option value="0">Selecione</option>
                                                {foreach $state_list as $state}
                                                <option value="{$state.idstate}">{$state.state_name}</option>
                                                {/foreach}
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address_city">Cidade</label>
                                        <div class="controls">
                                        <span class="carregando" style="display: none;">Aguarde, carregando...</span>
                                            <select class="span12" name="address_city" id="address_city">
                                                    <option value="0">-- Escolha um estado --</option>
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="district_iddistrict"> Tipo</label>
                                        <div class="controls">
                                            <select class="span12" id="district_iddistrict" name="district_iddistrict">
                                                <option value="1">Centro</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4>Contato</h4>
                            <hr class="separator" />
                            <div class="row-fluid">
                                <div class="span8 contacts">
                                    <fieldset class="contact">
                                    <div class="control-group">
                                        <label class="control-label" for="contact_type"> Tipo</label>
                                        <div class="controls">
                                            <select class="span12" id="contact_type[]" name="contact_type[]">
                                                <option value="0">Selecione</option>
                                                {foreach $type_list as $type}
                                                <option value="{$type.idcontact_type}">{$type.type_name}</option>
                                                {/foreach}
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="contact_value"> Valor</label>
                                        <div class="controls">
                                            <input type="text" id="contact_value[]" name="contact_value[]" class="span12" />
                                        </div>
                                    </div>
                                        <a href="#" class="removeField controls">Remover Telefone</a>
                                    </fieldset>
                                            
                                <div class="control-group">
                                    <div class="controls">
                                        <a href="#" class="addField">Adicionar Telefone</a>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="form-actions">
                                    <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Cadastrar</button>
                                    <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancelar</button>
                            </div>
                    </div>
            </div>
	</div>                                
        <div class="row-fluid">
            <div class="span11">
                        <div class="well" style="padding-bottom: 10px;">
                                <h4>Empresas Cadastradas</h4>
                                <hr class="separator" />
                                <div class="row-fluid">
                                    <div class="span11">
                                        <table cellpadding="0" cellspacing="0" border="0" class="dynamicTable table table-condensed table-primary table-vertical-center table-thead-simple">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nome</th>
                                                    <th>Descrição</th>
                                                    <th>Opções</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $enterprise_list as $ent}
                                                <tr class="gradeX">
                                                    <td>{$ent.identerprise}</td>
                                                    <td>{$ent.name}</td>
                                                    <td>{$ent.description}</td>
                                                    <td class="center">
                                                        <a href="http://{$smarty.server.SERVER_NAME}/painel/empresa/acao/editar/id/{$ent.identerprise}" class="btn-action glyphicons pencil btn-success"><i></i></a>
                                                        <a href="#" class="btn-action glyphicons remove_2 btn-danger"><i></i></a>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
        </div>
</form>
{include "dashboard/common/footer.tpl"}
