{include "site/common/header.tpl"}
    <!-- 404 Page -->
        <div class="span12">
            {if $error == '404'}
            <div class="error-page">
                <div class="row-fluid">
                    <div class="span8 offset2">
                        <h2>404</h2>
                        <h4>Desculpe, a página solicitada não existe.</h4>
                        <p>Por favor use o sistema de busca para tentar encontrar sua informação</p>
                        <p>Ou nos envie um email com o link quebrado.</p>
                        <br>
                        <a href="/" class="btn btn-large btn-success">Voltar à Home</a>
                    </div>
                </div>
            </div>
            {elseif $error == '500'}
            <div class="error-page">
                <div class="row-fluid">
                    <div class="span8 offset2">
                        <h2>500</h2>
                        <h4>Desculpe, ocorreu um erro em nossos servidores</h4>
                        <p>Por favor use o sistema de busca para tentar encontrar sua informação</p>
                        <p>Um email foi com o erro foi enviado à noss equipe de desenvolvimento, em breve solucionaremos este problema.</p>
                        <br>
                        <a href="/" class="btn btn-large btn-success btn-solid">Voltar à Home</a>
                    </div>
                </div>
            </div>
            <pre class="text-left">
                {$errorDetail}
            </pre>
            {/if}
        </div>
    </div>
</div>
<!-- End 404 Page -->

{include "site/common/footer.tpl"}