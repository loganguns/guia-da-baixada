{include "site/common/header.tpl"}

    <!-- Index Content -->
    <div class="span8">
        <div class="page-header">
            <h2><i class="icon-play-circle"></i> <span>Parceiros</span></h2>
        </div>
        
            Desconto de até 100% com os parceiros do Guia da Baixada para anunciantes do plano Digital III. Veja a lista dos parceiros do Guia da Baixada.
            Serviços Gráficos { Drop com nome das gráficas }
            Arino, Wan Digital, Print Master
            Serviços oferecidos: Folder, Flyer, Cartão de visita, Toldos, Envelopamento de carros.
            Soluções Web
            H1 Internet
            Serviços: Criação de site, Hospedagem de site e e-mails, Sistema de e-mail marketing.
            Monitoramento e Produção
            Net Eye
            Serviços: Software para controle de produtividade.
            Divulgação
            Jornal Araruama, X Mídia
            Se a sua empresa ainda não anuncia no Guia da Baixada, não perca tempo e faça o cadastro no Guia que mais cresce no Brasil.
            Caso precise de ajuda, contate um de nossos representantes.


        
    </div>
    <!-- End Index Content -->
    {include "site/common/asside.tpl"}
{include "site/common/footer.tpl"}
