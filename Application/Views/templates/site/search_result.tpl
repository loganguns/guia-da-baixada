{include "site/common/header.tpl"}
<div class="row">
    <!-- Results -->
    <div class="span8">
        <div class="page-header">
            <h2>Localizamos anúncio(s) com o termo <i class="icon-search"></i> <span> "{$searchParameters.searchTerm|default: ''}" {*em "{$searchParameters.local|default: 'Todas as Cidades'}"*}</span></h2>
        </div>
        <section id="guiaResultadoBusca">
            <div class="guiaResultadoBuscaGoogle">
                {*<script type="text/javascript"><!--
                    google_ad_client = "ca-pub-8575284190634177";
                    /* Guia - Resultado Busca Superior */
                    google_ad_slot = "3441412340";
                    google_ad_width = 728;
                    google_ad_height = 90;
                    //-->
                    </script>
                    <script type="text/javascript"
                    src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                </script>*}
                <img alt="Publicidade Provisória" src="/public/site/publicidade/h1internet-websites-665x105.jpg" />
            </div>
            
            <div id="anuncioLista">
                {if $announcements != null}
                {foreach $announcements as $curr}
                {*<li data-announcement-id="{$curr.idadvertising}" data-enterprise-id="{$curr.enterprise_identerprise}">
                    <div class="anuncioTitulo">{$curr.title} - {$curr.name}</div>
                    <div class="anuncioPrevia">{$curr.content}</div>
                    <div class="anuncioEndereco"><span>Endereço:</span> {$curr.address} nº {$curr.number} - {$curr.district_name} - {$curr.city_name} - {$curr.state_name}</div>
                    <div class="anuncioTelefone">Carregando... <img src="/public/site/img/294.gif" style="vertical-align: text-bottom" /></div>
                    <div class="btn btn-warning btn-contacts">Exibir Telefone</div>
                </li>*}
                {if $curr@iteration == '6'}
                <div class="guiaResultadoBuscaGoogle" style="border-top:1px solid #CCC;">
                    {*<script type="text/javascript"><!--
                        google_ad_client = "ca-pub-8575284190634177";
                        /* Guia - Busca */
                        google_ad_slot = "1993201948";
                        google_ad_width = 728;
                        google_ad_height = 90;
                        //-->
                        </script>
                        <script type="text/javascript"
                        src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                    </script>*}
                    <img alt="Publicidade Provisória" src="/public/site/publicidade/h1internet-websites-665x105.jpg" />
                </div>
                {/if}
                <!-- Anúncio #{$curr@iteration} idempresa: {$curr.identerprise} idanuncio: {$curr.idadvertising} -->
                <div class="guiaResultadoBuscaAnuncio" data-announcement-id="{$curr.idadvertising}" data-enterprise-id="{$curr.enterprise_identerprise}">
                    <h2>{if $curr.title > ''}{$curr.title} -{/if} {$curr.name}
                    <div class="pull-right">
                        Estrelas...
                    </div></h2>
                    <div class="anuncioConteudo">
                        {$curr.content}
                    </div>
                    <div class="pull-left">
                        {*<address>
                            <span>Endereço:</span> {$curr.address} nº {$curr.number} - {$curr.district_name} - {$curr.city_name} - {$curr.state_name}
                        </address>*}
                    </div>
                    <div id="anuncioTelefone_{$curr.identerprise}" name="anuncioTelefone_{$curr.identerprise}" class="anuncioTelefone">
                        Carregando... <img src="/public/site/img/294.gif" style="vertical-align: text-bottom" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="pull-right anuncioOpcoes">
                        <ul class="unstyled inline">
                            <li data-announcement-id="{$curr.idadvertising}" data-enterprise-id="{$curr.enterprise_identerprise}"><div class="btn btn-warning btn-contacts">Ver Contatos</div></li>
                            <li data-announcement-id="{$curr.idadvertising}" data-enterprise-id="{$curr.enterprise_identerprise}"><div class="btn btn-info btn-adresses">Endereços</div></li>
                            <li><a href="{$curr.name}"><div class="btn btn-info">+ Detalhes</div></a></li>
                            {*<li><a href="#" class="btn btn-warning">+ Detalhes</a></li>
                            <li><a href="#" class="btn btn-warning">Ligue para mim</a></li>*}
                        </ul>
                    </div>
                </div>
                <!-- End Anúncio #{$curr@iteration} idempresa: {$curr.identerprise} idanuncio: {$curr.idadvertising} -->
                {/foreach}
                {else}
                <div class="guiaResultadoBuscaAnuncio">
                    Nenhum resultado...
                </div>   
                {/if}
            </div>
            
            <div class="guiaResultadoBuscaGoogle">
                {*<script type="text/javascript"><!--
                    google_ad_client = "ca-pub-8575284190634177";
                    /* Guia - Resultado Busca Superior */
                    google_ad_slot = "3441412340";
                    google_ad_width = 728;
                    google_ad_height = 90;
                    //-->
                    </script>
                    <script type="text/javascript"
                    src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                </script>*}
                <img alt="Publicidade Provisória" src="/public/site/publicidade/h1internet-websites-665x105.jpg" />
            </div>

            <div class="guiaResultadoBuscaGooglePaginacao pagination">
                <ul>
                    <li><a href="#">Anterior</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Próximo</a></li>
                </ul>
            </div>

            <div id="guiaResultadoBuscaCadastroNovo">
                A sua empresa não está aqui? <a data-toggle="modal" data-target="#ModalAnuncio" href="#">Cadastre gratuitamente no Guia da Baixada</a> em 6 etapas.
            </div>                            

        </section>
    </div>
    <!-- End Results -->                                             
    {include "site/common/asside.tpl"}
</div>
{include "site/common/footer.tpl"}