<!-- Modal -->
<div id="ModalCadastro" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="ModalCadastro" aria-hidden="true" style="color: black">
    <form action="/cadastro/" method="post" class="form-horizontal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>Cadastro Básico - Guia da Baixada</h3>
        </div>
        <div class="modal-body">
            <p id="advise"></p>
            <div class="control-group">
                <!--[if lt IE 10]><label class="control-label" for="new-name">Digite seu nome:</label><![endif]-->
                <div class="control">
                    <input type="text" id="new-name" name="new-name" placeholder="Nome Ex.: José Antonio da Silva" style="width: 97%; height: 35px; font-size: 25px;" value="{$new_name|default: ""}" required>
                </div>
            </div>
            <div class="control-group">
                <!--[if lt IE 10]><label class="control-label" for="new-email">Digite seu email:</label><![endif]-->
                <div class="control">
                    <input type="text" id="new-email" name="new-email" placeholder="Email Ex.: jose@email.com" style="width: 97%; height: 35px; font-size: 25px;" value="{$new_email|default: ""}" type="email" required>
                </div>
            </div>
            <div class="control-group">
                <!--[if lt IE 10]><label class="control-label" for="new-pass">Digite uma senha:</label><![endif]-->
                <div class="control">
                    <input type="password" id="new-pass" name="new-pass" placeholder="Digite uma senha" style="width: 97%; height: 35px; font-size: 25px;" required>
                </div>
            </div>
            <div class="control-group">
                <!--[if lt IE 10]><label class="control-label" for="new-pass2">Repita sua senha:</label><![endif]-->
                <div class="control">
                    <input type="password" id="new-pass2" name="new-pass2" placeholder="Confirme sua senha" style="width: 97%; height: 35px; font-size: 25px;" required>
                </div>
            </div>
        </div> 
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true" type="reset">Cancelar</button>
            <button class="btn btn-success" value="Cadastrar" type="submit">Cadastrar</button>
        </div>     
    </form>
</div>
<!-- Modal -->  