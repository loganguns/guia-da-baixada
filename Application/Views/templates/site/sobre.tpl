{include "site/common/header.tpl"}

    <!-- Index Content -->
    <div class="span8">
        <div class="page-header">
            <h2><i class="icon-play-circle"></i> <span>Sobre o Guia</span></h2>
        </div>
        
            O Portal Guia da Baixada tem como objetivo ter o cadastro de todos os comércios do Brasil.
            Contamos com profissionais que possuem experiência em desenvolvimento Web e Marketing Digital.
            Qual é a diferença deste Guia Online para os que já existem na internet?
            O Guia da Baixada foi desenvolvido com o que há de mais moderno em estruturação Web. Tomamos cuidado para que o Portal não se torne um “Site de Banners”, no qual o usuário acaba se perdendo com tantos banners em destaques e “pisca-pisca”.
            Para encontrar um serviço e/ou produto é simples, basta digitar em nossa busca o que deseja que o sistema irá buscar o termo desejado em nosso banco de dados.
            Por que anunciar?
            Não importa qual é a sua área de atuação, o Guia da Baixada é útil para todos os profissionais: autônomo, comércio, indústria, etc.
            Estamos presentes nas principais Redes Sociais. A estruturação do Portal foi pensando no usuário e também nos buscadores como Google, Yahoo, Bing, entre outros.
            O melhor de tudo isso é que para ter a sua empresa em nosso Portal você não precisa pagar mensalidades.
            Conheça os planos para anunciar no Guia da Baixada.

        
    </div>
    <!-- End Index Content -->
    {include "site/common/asside.tpl"}
{include "site/common/footer.tpl"}
