{include "site/common/header.tpl"}

    <style type="text/css">
        #map {
            height: 400px;
        }
        .bs-docs-example {
            width: 700px;
        }
    </style>
    
    {if $announcements != null}
        {foreach $announcements as $curr}
    <div class="page-header">
            <h1>{$curr.name}</h1>
    </div>
<div class="">
    <div id="map" class="map"></div>
</div>
        {/foreach}  
    {/if}
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="/public/site/js/vendor/jquery.gmap3.min.js"></script>
<script type="text/javascript">
		$('#map').gmap3({
					map: {
						options: {
							center: [46.578498, 2.457275],
							zoom: 5
						}
					},
					marker: {
						values: [{
							latLng: [48.8620722, 2.352047],
							data: 'Paris !'
						}, {
							address: '86000 Poitiers, France',
							data: 'Poitiers : great city !'
						}, {
							address: '66000 Perpignan, France',
							data: 'Perpignan ! <br> GO USAP !',
							options: {
								icon: 'http://maps.google.com/mapfiles/marker_green.png'
							}
						}],
						options: {
							draggable: false
						},
						events: {
							click: function(marker, event, context) {
								var map = $(this).gmap3('get'),
									infowindow = $(this).gmap3({
										get: {
											name: 'infowindow'
										}
									});
								if (infowindow) {
									infowindow.open(map, marker);
									infowindow.setContent(context.data);
								} else {
									$(this).gmap3({
										infowindow: {
											anchor: marker,
											options: {
												content: context.data
											}
										}
									});
								}
							}
						}
					}
				});
	</script>
{include "site/common/footer.tpl"}