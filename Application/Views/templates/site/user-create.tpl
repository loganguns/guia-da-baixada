{include "site/common/header.tpl"}
{if $create === 'already'}
    <script>
        $('#ModalCadastro').modal('toggle');
        $('<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button><strong></strong> O email informado já se encontra em nosso banco de dados. Utilize outro email ou tente recuperar seus dados de acesso.</div>').appendTo('#advise');
    </script>
{elseif $create === 'error'}
    <script>
        $('#ModalCadastro').modal('toggle');
        $('<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button><strong></strong> Ocorreu um erro inesperado em nosso sitema. Tente efetuar o cadastro mais tarde.</div>').appendTo('#advise');
    </script>
{elseif $create === 'invalid_mail'}
    <script>
        $('#ModalCadastro').modal('toggle');
        $('<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button><strong></strong> O email informado é inválido.</div>').appendTo('#advise');
    </script>
{elseif $create === 'success'}
    <!-- Modal -->
    <div id="ModalCadastroSuccess" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="color: black">
        <form action="/cadastro/" method="post" class="form-horizontal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Cadastro Básico - Guia da Baixada</h3>
            </div>
            <div class="modal-body">
                Conta criada..
            </div> 
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true" type="reset">Fechar</button>
            </div>
    </div>
    <!-- Modal --> 
    <script>
        $('#ModalCadastroSuccess').modal('toggle');
    </script>
{/if}
{include "site/common/footer.tpl"}