{if $smarty.get.ativado|default: ''}
<!-- Modal -->
<div id="ModalAtivacao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAtivacao" aria-hidden="true" style="color: black">
    <form action="/cadastro/" method="post" class="form-horizontal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>Ativação de Conta</h3>
        </div>
        <div class="modal-body">
            {if $smarty.get.ativado === 'true'}Conta ativada{else}Error! Código não encontrado, codigo velho (conta ja ativa) ou erro geral. {/if}
        </div> 
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true" type="reset">Fechar</button>
        </div>     
    </form>
</div>
<script>
    $('#ModalAtivacao').modal('show');
</script>
<!-- Modal -->        
{/if}