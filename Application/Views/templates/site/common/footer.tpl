                                                    </div>
                                                </div>
						<!-- End Content -->
					</div>
				</div>
				<a href="#" class="scroll-top"><i class="icon-chevron-up"></i></a>
			</div>
		</div>
		<!-- Ticker -->
		{*<div class="ticker">
			<div class="container">
				<div class="row">
					<div class="span12">
						<div id="flexslider-ticker" class="tweet flexslider" data-selector=".tweet_list > li" data-animation="slide" data-direction="vertical" data-controlnav="false">
							<i class="icon-refresh icon-spin"></i> Carregando Tweets...
						</div>
					</div>
				</div>
			</div>
		</div>*}
		<!-- End Ticker -->
		<!-- Footer -->
		<footer>
			<!-- Footer Block -->
			<div class="footer">
				<div class="container">
					<div class="row">
						<div class="span3">
							<!-- Logo -->
							<div class="logo">
								<a href="#"><img id="logo-footer" src="/public/site/images/logos/guia-da-baixada-logo-footer.png" alt="O Guia da Baixada" /></a>
							</div>
							<!-- End Logo -->
						</div>
						<div class="span2">
							<!-- Menus -->
							<h3><i class="icon-plus-sign"></i> Anuncie Grátis</h3>
							<h3><i class="icon-info-sign"></i> O Guia</h3>
							<ul class="menu-footer unstyled suport">
                                                            <li><a href="/representantes"><i class="icon-globe"></i> Representantes</a></li>
                                                            <li><a href="/parceiros"><i class="icon-bullhorn"></i> Parceiros</a></li>
							</ul>
							<!-- End Menus -->
						</div>
						<div class="span3">
							<!-- About us -->
							<h3><i class="icon-book"></i> Como Funciona</h3>
							<p>Se você ou sua empresa presta algum serviço, cadastre seu anúncio gratuitamente. Faça parte do Guia da Baixada.</p>
							<div class="count">
								<div class="pull-left">
									<h6><i class="icon-building"></i> Empresas</h6>
									<h4>Mais de 500</h4>
								</div>
								<div class="pull-left">
									<h6><i class="icon-user"></i> Usuários</h6>
									<h4>Mais de 100</h4>
								</div>
							</div>
							<!-- End About us -->
						</div>
						<div class="span4">
							{*<h3><i class="icon-comments-alt"></i> Depoimentos</h3>
							<!-- Testimonials Widget -->
							<div id="testimonials" class="testimonials flexslider" data-controlnav="false" data-animationloop="false" data-directionnav="true">
                                                            <ul class="slides unstyled">
                                                                <li>
                                                                    <blockquote>
                                                                        <p><i class="icon-quote-left"></i> Aqui entrará a descrição do depoimento da pessoa!</p>
                                                                        <small><cite title="Source Title">Nome da Pessoa</cite>, Empresa</small>
                                                                    </blockquote>
                                                                </li>
                                                                <li>
                                                                    <blockquote>
                                                                        <p><i class="icon-quote-left"></i> Aqui entrará a descrição do depoimento da pessoa! 2</p>
                                                                        <small><cite title="Source Title">Nome da Pessoa</cite>, Empresa</small>
                                                                    </blockquote>
                                                                </li>
                                                            </ul>
							</div>
							<!-- End Testimonials Widget -->*}
							<!-- Social Widget -->
							<ul class="social-icons inline">
                                                            <li><a href="https://www.facebook.com/CatalogodaRegiao" target="_blank" data-toggle="tooltip" title="Facebook" class="facebook"><i class="icon-facebook"></i></a></li>
                                                            <li><a href="https://twitter.com/catalogoregiao" target="_blank" data-toggle="tooltip" title="Twitter" class="twitter"><i class="icon-twitter"></i></a></li>
                                                            {*<li><a href="#" data-toggle="tooltip" title="Google+" class="google-plus"><i class="icon-google-plus"></i></a></li>*}
                                                            <li><a href="mailto:contato@catalogodaregiao.com.br" target="_blank" data-toggle="tooltip" title="E-mail" class="email"><i class="icon-envelope"></i></a></li>
                                                            {*<li><a href="#" data-toggle="tooltip" title="RSS" class="rss"><i class="icon-rss"></i></a></li>*}
							</ul>
							<!-- End Social Widget -->
							<!-- Social Buttons Widget -->
							<ul class="social-buttons unstyled inline">
                                                            <li class="twitter">
                                                                <a href="https://twitter.com/share" class="twitter-share-button" data-via="catalogoregiao" data-lang="pt" data-hashtags="catalogodaregiao">Tweetar</a>
                                                                <script>!function(d,s,id){ var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){ js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                                            </li>
                                                            <li class="facebook">
                                                                {*<div class="fb-like" data-href="http://www.catalogodaregiao.com.br/" data-send="false" data-layout="button_count" data-width="90" data-show-faces="false"></div>*}
                                                                <div id="fb-root"></div>
                                                                <script>(function(d, s, id) {
                                                                  var js, fjs = d.getElementsByTagName(s)[0];
                                                                  if (d.getElementById(id)) return;
                                                                  js = d.createElement(s); js.id = id;
                                                                  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=406431852784455";
                                                                  fjs.parentNode.insertBefore(js, fjs);
                                                                }(document, 'script', 'facebook-jssdk'));</script>
                                                                <div class="fb-like" data-href="http://www.facebook.com/CatalogodaRegiao" data-width="450" data-layout="button_count" data-show-faces="true" data-send="true"></div>
                                                            </li>
                                                            <li class="google-plus">
                                                                <div class="g-plusone" data-size="medium" data-align="left" data-href="http://catalogodaregiao.com.br/"></div>
                                                            </li>
							</ul>
							<!-- End Social Buttons Widget -->
						</div>
					</div>
				</div>
			</div>
			<!-- End Footer Block -->
			<!-- Copyright -->
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="span5">
                                                    <p>
                                                        Copyright &copy; {$smarty.now|date_format:"%Y"} O Guia da Baixada - Todos os Direitos Reservados. <span class="author">
                                                        <small><br />Desenvolvido por <a href="#" target="_blank">WeDev Brasil Soluções Web</a></small></span>
                                                    </p>
						</div>
						<div class="span7">
							<div class="hidden-phone">
								<!-- Footer Menu -->
								<ul class="unstyled">
									<li class="active"><a href="/">Home</a></li>
									<li><a href="/sobre">Sobre O Guia</a></li>
									<li><a data-toggle="modal" data-target="#ModalAnuncio" href="#">Anuncie Grátis</a></li>
									<li><a href="/representantes">Representantes</a></li>
									<li><a href="/parceiros">Parceiros</a></li>
								</ul>
								<!-- End Footer Menu -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Copyright -->
		</footer>
		<!-- End Footer -->
	</div>
	
	{include "site/common/modal_freeAnnouncement.tpl"}
        
	<script type="text/javascript" src="/public/site/js/vendor/jquery.cookie.min.js"></script>
	{*<script type="text/javascript" src="/public/site/js/vendor/jquery.nicescroll.min.js"></script>*}
	<script type="text/javascript" src="/public/site/js/vendor/jquery.parallax.min.js"></script>
	<script type="text/javascript" src="/public/site/js/vendor/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="/public/site/js/vendor/jquery.autosize.min.js"></script>
	<script type="text/javascript" src="/public/site/js/vendor/jquery.easing.min.js"></script>
	<script type="text/javascript" src="/public/site/js/vendor/prettify.js"></script>
        <script type="text/javascript" src="/public/site/js/vendor/jquery.cookie.min.js"></script>
        
        {*<script type="text/javascript" src="/public/site/js/geolocalization.js"></script>*}

        <script type="text/javascript" src="/public/site/js/jquery.smartWizard.js"></script>
        <script type="text/javascript" src="/public/site/js/magicsuggest-1.3.0.js"></script>
        
	<!--[if lte IE 9]>
		<script type="text/javascript" src="/public/site/js/vendor/jquery.placeholder.min.js"></script>
		<script type="text/javascript" src="/public/site/js/vendor/jquery.menu.min.js"></script>
		<script type="text/javascript">
			/* <![CDATA[ */
			$(function()  { 
				$('#nav .menu').menu( { 'effect' : 'fade'});
			});
			/* ]]> */
		</script>
	<![endif]-->
	<script type="text/javascript" src="/public/site/js/plugins.js"></script>
	<script type="text/javascript" src="/public/site/js/main.js"></script>
	<script type="text/javascript" src="/public/site/js/custom.js"></script>
        
	
        
        {*<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>*}
        {*<script type="text/javascript" src="/public/site/js/jquery-1.4.2.min.js"></script>*}

        {*<!-- Google Analytics -->
	<script>
          (function(i,s,o,g,r,a,m){ i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        
          ga('create', 'UA-41303033-1', 'catalogodaregiao.com.br');
          ga('send', 'pageview');
        
        </script>
        <!-- End Google Analytics -->*}
        
</body>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
</html>
