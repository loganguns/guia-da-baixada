<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <!-- Dinamic Meta -->
        <title>O Guia da Baixada</title>
        <meta name="Description" content="Guia da Baixada" />
        <meta name="keywords" content="Guia da Baixada" />
        <meta name="revisit-after" content="3" />
        <!-- End Dinamic Metas -->

        <meta name="Author" content="WeDev Brasil" />
        <meta name="robots" content="index, follow" />

        <link rel="stylesheet" type="text/css" href="/public/site/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/bootstrap-responsive.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" type="text/css" href="/public/site/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/font-awesome-corp.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/font-awesome-ext.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/font-awesome-social.css">
        <!-- End Font Awesome -->
        <!-- Menu Styles -->
        <link rel="stylesheet" type="text/css" href="/public/site/css/menu/core.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/menu/styles/lsteel-blue-OLD.css">
        <!--[if (gt IE 9)|!(IE)]><!-->
        <link rel="stylesheet" type="text/css" href="/public/site/css/menu/effects/fading.css" media="screen">
        <link rel="stylesheet" type="text/css" href="/public/site/css/menu/effects/slide.css" media="screen">
        <!--<![endif]-->
        <!-- End Menu Styles -->
        <link rel="stylesheet" type="text/css" href="/public/site/css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/prettify.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/docs.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/prettyPhoto.css">
        {*<link rel="stylesheet" type="text/css" href="/public/site/css/switcher.css">*}
        <link rel="stylesheet" type="text/css" href="/public/site/css/main.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/custom.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/skins/cyan.css" id="custom_style">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,400italic|Arvo:400,400italic,700|Lato:400,700,400italic,900|Vollkorn:400,700,400italic|Ubuntu:400,400italic,500,700|Droid+Sans:400,700|Prociono|Lora:400,400italic,700|Merriweather:400,700|Bitter:400,400italic,700|Kreon:400,700|Raleway:400,600|Quicksand:400,700|Oswald:400,700,300|Source+Sans+Pro:400,400italic,600,700,900|Droid+Serif:400,700,400italic">
        <link rel="stylesheet" type="text/css" href="/public/site/css/smart_wizard_vertical.css">
        <link rel="stylesheet" type="text/css" href="/public/site/css/magicsuggest-1.3.0.css">
        <!--[if IE]><link rel="stylesheet" type="text/css" href="/public/site/css/ie.css"><![endif]-->
        <!--[if IE 7]><link rel="stylesheet" type="text/css" href="/public/site/css/font-awesome-ie7.min.css"><![endif]-->
        <!--[if IE 7]><link rel="stylesheet" type="text/css" href="/public/site/css/font-awesome-more-ie7.min.css"><![endif]-->
        <!--[if (gte IE 6)&(lte IE 8)]><script type="text/javascript" src="/public/site/js/vendor/selectivizr.min.js"></script><![endif]-->
        <!--[if lt IE 9]><script type="text/javascript" src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->

        <link rel="shortcut icon" href="/public/site/favicon.ico">
        <script type="text/javascript" src="/public/site/js/vendor/modernizr.min.js"></script>
        <script type="text/javascript" src="/public/site/js/vendor/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="/public/site/js/vendor/bootstrap.min.js"></script>
        <script type="text/javascript" src="/public/site/js/vendor/retina.js"></script>
        
        {*<script src="https://optin.entregaemails.com.br/forms/5214b04338a582b3a70004f6" type="text/javascript" charset="utf-8" async defer></script>*}
    </head>
    <body class="wide">
        <div class="layout">
            <!-- Main Header -->
            <header>
                <!-- Top Bar -->
                <div class="top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="span12">
                                <!-- Access -->
                                <ul class="access unstyled pull-right">
                                    <li class="register">Não encontrou sua empresa? <a data-toggle="modal" data-target="#ModalAnuncio" href="#">Cadastre</a></li>
                                    <li class="dropdown login-area">
                                        <a href="index.php#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> Login</a>
                                        <div class="dropdown-menu">
                                            <form action="index.php#">
                                                <div class="control-group">
                                                    <label class="control-label" for="inputEmail">Email</label>
                                                    <div class="controls">
                                                        <div class="input-prepend">
                                                            <span class="add-on"><i class="icon-envelope"></i></span>
                                                            <input class="input-large" type="text" id="inputEmail" placeholder="Digite seu email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="inputPassword">Password</label>
                                                    <div class="controls">
                                                        <div class="input-prepend">
                                                            <span class="add-on"><i class="icon-key"></i></span>
                                                            <input class="input-large" type="password" id="inputPassword" placeholder="Digite sua senha">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <label class="checkbox">
                                                            <input type="checkbox"> Lembrar
                                                        </label>
                                                        <div class="pull-left">
                                                            <a href="index.php#" class="link"><i class="icon-exclamation-sign"></i> Esqueceu sua senha?</a>
                                                        </div>
                                                        <div class="pull-right">
                                                            <button type="submit" class="btn">Login</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                                <!-- End Access -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Top Bar -->
                <!-- Header -->
                <div class="header">
                    <div class="container">
                        <div class="row">
                            <div class="span4">
                                <!-- Logo -->
                                <div class="logo">
                                    <a href="/"><img id="logo" src="/public/site/images/logos/guia-da-baixada-logo.png" alt="O Guia da Baixada"></a>
                                    <h2 class="hide-text">O Guia da Baixada</h2>
                                </div>
                                <!-- End Logo -->
                            </div>
                            <div class="span8">
                                <!-- Big Search -->
                                <div class="big-search">
                                    {*<form id="main_form_search" name="main_form_search" action="/{$smarty.cookies.state|default:'todos-estados'|lower|replace:' ':'-'}/{$smarty.cookies.city|default:'todas-cidades'|lower|replace:' ':'-'}/" class="form-inline" method="post">*}
                                    <form id="main_form_search" name="main_form_search" action="/" class="form-inline" method="post">
                                        <div class="row-fluid">
                                            <span class="span5"></span>
                                            <div class="span5">
                                                <label for="searchTerm">O que você procura?</label>
                                                <input id="searchTerm" name="searchTerm" class="input-block-level" type="text" value="{$searchParameters.searchTerm|default: ''}" placeholder="Bares, Restaurantes, Informática">
                                                <span class="help-block"><small class="visible-desktop"><i class="icon-circle-arrow-right"></i> Digite um produto ou serviço</small></span>
                                            </div>
                                            <div class="span2">
                                                <label style="display:block;" class="hidden-phone">&nbsp;</label>
                                                <input type="submit" class="btn btn-large btn-inverse btn-search" value="Buscar" />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- End Big Search -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Header -->
                <!-- Navigation -->
                <nav id="nav" class="navigation lsteel-blue">
                    <div class="container">
                        <div class="row">
                            <div class="span12">
                                <ul class="menu fading">
                                    <li class="active"><a href="/">Home</a></li>
                                    <li><a href="/sobre">Sobre O Guia da Baixada</a></li>
                                    <li><a data-toggle="modal" data-target="#ModalAnuncio" href="#">Anuncie Grátis</a></li>
                                    <li><a href="/representantes">Seja um Representante</a></li>
                                    <li><a href="/parceiros">Parceiros</a></li>
                                </ul>
                                {*<div class="pull-right">
                                    <div id="language" class="language visible-desktop">
                                        <a href="#" class="current"><i class="icon-globe"></i> <span id='city_name'>{$city|default:'Todas as Cidades'}</span> <i class="icon-angle-down"></i></a>
                                        <div class="language-selector">
                                            <div class="page-header">
                                                <h5><i class="icon-globe"></i> Escolha sua cidade <span class="pull-right"><a href="#" class="close"><i class="icon-remove-sign"></i></a></span></h5>
                                            </div>
                                            <ul class="unstyled">
                                                <li><a href="#"> Araruama</a></li>
                                                <li><a href="#"> São Pedro</a></li>
                                                <li><a href="#"> Cabo Frio</a></li>
                                                <li><a href="#"> Iguaba</a></li>
                                                <li><a href="#"> Búzios</a></li>
                                                <li><a href="#"> Arraial do C.</a></li>
                                                <li><a href="#"> Rio das Ostras</a></li>
                                                <li><a href="#"> Macaé</a></li>
                                            </ul>
                                            <hr>
                                            <p><a href="/representantes">Ajude a levar O Guia da Baixada até a sua região, seja um representante</a></p>
                                        </div>
                                    </div>
                                </div>*}
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- End Navigation -->
                <!-- Search -->
                {*<div class="search">
                    <div class="container">
                        <div class="row">
                            <div class="span6">
                                <!-- Breadcrumb -->
                                <ul class="breadcrumb hidden-phone">
                                    <li><span class="divider"><a href="/" data-toggle="tooltip" title="Ir para página inicial"><i class="icon-home"></i></a></span></li>
                                    <li><span>Home <span class="divider">></span></span></li>
                                    <li><span>Rio de Janeiro <span class="divider">></span></span></li>
                                    <li><span>Araruama <span class="divider">></span></span></li>
                                    <li class="active"><span>Internet</span></li>
                                </ul>
                                <!-- End Breadcrumb -->
                            </div>
                        </div>
                    </div>
                </div>*}
                <!-- End Search -->
            </header>
            <!-- End Main Header -->
            <div class="main">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <!-- Content -->
                            <div class="content">
                                <div class="row">
                                    <div id="geo_status"></div>