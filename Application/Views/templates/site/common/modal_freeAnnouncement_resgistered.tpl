{if $smarty.get.cadastrogratis|default: ''}
<!-- Modal -->
<div id="ModalCadastroGratis" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalCadastroGratis" aria-hidden="true" style="color: black">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3>Anúncio Grátis - Guia da Baixada</h3>
    </div>
    <div class="modal-body">
        {if $smarty.get.cadastrogratis === 'true'}Cadastro efetuado com sucesso. Em breve iremos analizar seu pedido e cadastra-lo em nosso sistema. Obrigado!
        {else}Error! Não foi possível efetuar seu cadastro ou erro geral. {/if}
    </div> 
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true" type="reset">Fechar</button>
    </div>
</div>
<script>
    $('#ModalCadastroGratis').modal('show');
</script>
<!-- Modal -->        
{/if}