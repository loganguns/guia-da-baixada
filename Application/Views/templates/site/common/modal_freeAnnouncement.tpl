<div id="ModalAnuncio" class="modal hide fade bigModal" tabindex="-1" role="dialog" aria-labelledby="ModalCadastro" aria-hidden="true" style="color: black">
    <form id="formModalAnuncio" action="/portal/anunciogratis" method="POST">
        <div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Anúncio Grátis - Guia da Baixada</h3>
            </div>
            <div>
                <!-- Smart Wizard -->
                                <div id="wizard" class="swMain" style="padding: 20px">
                                    <ul>
                                    <li><a href="#step-1">
                                        <label class="stepNumber">1&ordm;</label><![endif]-->
                                        <span class="stepDesc">
                                           &nbsp;&nbsp;&nbsp;Passo<br />
                                           <small>Detalhes da Empresa</small>
                                        </span>
                                    </a></li>
                                    <li><a href="#step-2">
                                        <label class="stepNumber">2&ordm;</label><![endif]-->
                                        <span class="stepDesc">
                                           &nbsp;&nbsp;&nbsp;Passo<br />
                                           <small>Detalhes do Endere&ccedil;o</small>
                                        </span>
                                    </a></li>
                                    <li><a href="#step-3">
                                        <label class="stepNumber">3&ordm;</label><![endif]-->
                                        <span class="stepDesc">
                                           &nbsp;&nbsp;&nbsp;Passo<br />
                                           <small>Detalhes de Contato</small>
                                        </span>                   
                                     </a></li>
                                    <li><a href="#step-4">
                                        <label class="stepNumber">4&ordm;</label><![endif]-->
                                        <span class="stepDesc">
                                            &nbsp;&nbsp;&nbsp;Passo<br />
                                           <small>Destalhes do An&uacute;ncio</small>
                                        </span>                   
                                    </a></li>
                                    <li><a href="#step-5">
                                        <label class="stepNumber">5&ordm;</label><![endif]-->
                                        <span class="stepDesc">
                                            &nbsp;&nbsp;&nbsp;Passo<br />
                                           <small>Prévia do An&uacute;ncio</small>
                                        </span>                   
                                    </a></li>
                                    <li><a href="#step-6">
                                        <label class="stepNumber">6&ordm;</label><![endif]-->
                                        <span class="stepDesc">
                                            &nbsp;&nbsp;&nbsp;Passo<br />
                                           <small>Confirmação</small>
                                        </span>                   
                                    </a></li>
                                        </ul>
                                    <div id="step-1" style="height: 450px">
                                <h2 class="StepTitle">Preencha com os dados da sua empresa</h2>
                                <br />

                                <div class="input-append control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Empresa:</label><![endif]-->

                                    <input type="text" id="enterprise_name" name="enterprise_name" placeholder="Nome da empresa Ex.: H1 Internet" style="width: 208%; height: 45px; font-size: 25px;" required>
                                    <button class="btn btn-info announcement-tooltip" type="button" data-toggle="popover" data-placement="bottom" data-content="Digite o nome fantasia da sua empresa ou estabelecimento comercial" title="" style="height: 55px;">?</button>
                                </div>
                                <span id="msg_enterprise_name"></span>


                                <div class="control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Categoria:</label><![endif]-->
                                    <select id="category" name="category" style="width: 100%; height: 55px; font-size: 25px;">
                                        <option>Selecione a categoria de atividade comercial</option>
                                    </select>
                                </div> 
                                <span id="msg_category"></span>
                                <div class="control-group">
                                    <div id="ms1" style="width: 100%; height: 55px; font-size: 25px;"></div>     
                                </div>

                            </div>
                            <div id="step-2" style="height: 450px">
                                <h2 class="StepTitle">Preencha com os dados do emdere&ccedil;o da sua empresa</h2>
                            <br />

                                <div class="input-append control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <input type="text" id="address" name="address" placeholder="Rua, Avenida, Rodovia. Ex.: Rovodia Amaral Peixoto" style="width: 672px; height: 45px; font-size: 25px;" required>
                                    <button class="btn btn-info announcement-tooltip" type="button" data-toggle="popover" data-placement="bottom" data-content="Digite o endereço de sua empresa" title="" style="height: 55px;">?</button>
                                </div>

                                <div  class="control-group">
                                    <div class="input-prepend">
                                        <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                        <span class="add-on" style="height: 45px; font-size: 25px">&nbsp;n&ordm;.:</span>
                                        <input type="text" id="address_number" name="address_number" placeholder="Número" style="width: 230px; height: 45px; font-size: 25px;" required>
                                    </div>

                                    <div class="input-append">
                                        <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                        <input type="text" id="address_complement" name="address_complement" placeholder="Complemento Ex.: Sala 01 Sobreloja" style="width: 230px; height: 45px; font-size: 25px;" required>
                                        <button class="btn btn-info announcement-tooltip" type="button" data-toggle="popover" data-placement="bottom" data-content="Digite o complemente do endereço" title="" style="height: 55px;">?</button>
                                    </div>
                                </div>

                                <div  class="control-group">
                                    <div class="input-prepend">
                                        <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                        <span class="add-on" style="height: 45px; font-size: 25px">CEP:</span>
                                        <input type="text" id="address_cep" name="address_cep" placeholder="CEP" style="width: 140px; height: 45px; font-size: 25px;" required>
                                    </div>

                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <select id="address_state" name="address_state" style="width: 262px; height: 55px; font-size: 25px;">
                                        <option value="0">Estado</option>
                                        <option value="1">Acre</option>
                                        <option value="2">Alagoas</option>
                                        <option value="3">Amazonas</option>
                                        <option value="4">Amapá</option>
                                        <option value="5">Bahia</option>
                                        <option value="6">Ceará</option>
                                        <option value="7">Distrito Federal</option>
                                        <option value="8">Espírito Santo</option>
                                        <option value="9">Goiás</option>
                                        <option value="10">Maranhão</option>
                                        <option value="11">Minas Gerais</option>
                                        <option value="12">Mato Grosso do Sul</option>
                                        <option value="13">Mato Grosso</option>
                                        <option value="14">Pará</option>
                                        <option value="15">Paraíba</option>
                                        <option value="16">Pernambuco</option>
                                        <option value="17">Piauí</option>
                                        <option value="18">Paraná</option>
                                        <option value="19">Rio de Janeiro</option>
                                        <option value="20">Rio Grande do Norte</option>
                                        <option value="21">Rondônia</option>
                                        <option value="22">Roraima</option>
                                        <option value="23">Rio Grande do Sul</option>
                                        <option value="24">Santa Catarina</option>
                                        <option value="25">Sergipe</option>
                                        <option value="26">São Paulo</option>
                                        <option value="27">Tocantins</option>
                                    </select>

                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <div class="carregando hide">Carregando... <img src="/public/site/img/294.gif" style="vertical-align: text-bottom" /></div>
                                    <select id="address_city" name="address_city" style="width: 225px; height: 55px; font-size: 25px;">
                                        <option>Cidade</option>
                                    </select>
                                </div>
                                <div class="input-append control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <input type="text" id="address_district" name="address_district" placeholder="Bairro Ex.: Coqueiral" style="width: 672px; height: 45px; font-size: 25px;" required>
                                    <button class="btn btn-info announcement-tooltip" type="button" data-toggle="popover" data-placement="bottom" data-content="Digite o bairo da sua empresa" title="" style="height: 55px;">?</button>
                                </div>
                            </div>                      
                            <div id="step-3" style="height: 450px">
                            <h2 class="StepTitle">Preencha com os dados de contato da sua empresa</h2>
                            <br />
                                <div  class="control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <select id="contact_type1" name="contact_type1" style="height: 55px; font-size: 25px">
                                        <option>Telefone Fixo</option>
                                        <option>Telefone Celular</option>
                                        <option>Nextel</option>
                                        <option>Email</option>
                                        <option>WebSite</option>
                                    </select>
                                    <input type="text" id="value1" name="value1" placeholder="Contato" style="width: 450px; height: 45px; font-size: 25px;" required>
                                </div>      
                                <div  class="control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <select id="contact_type2" name="contact_type2" style="height: 55px; font-size: 25px">
                                        <option>Telefone Fixo</option>
                                        <option selected="selected">Telefone Celular</option>
                                        <option>Nextel</option>
                                        <option>Email</option>
                                        <option>WebSite</option>
                                    </select>
                                    <input type="text" id="value2" name="value2" placeholder="Contato" style="width: 450px; height: 45px; font-size: 25px;" required>
                                </div>
                                <div  class="control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <select id="contact_type3" name="contact_type3" style="height: 55px; font-size: 25px">
                                        <option>Telefone Fixo</option>
                                        <option>Telefone Celular</option>
                                        <option selected="selected">Nextel</option>
                                        <option>Email</option>
                                        <option>WebSite</option>
                                    </select>
                                    <input type="text" id="value3" name="value3" placeholder="Contato" style="width: 450px; height: 45px; font-size: 25px;" required>
                                </div>
                                <div  class="control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <select id="contact_type4" name="contact_type4" style="height: 55px; font-size: 25px">
                                        <option>Telefone Fixo</option>
                                        <option>Telefone Celular</option>
                                        <option>Nextel</option>
                                        <option selected="selected">Email</option>
                                        <option>WebSite</option>
                                    </select>
                                    <input type="text" id="value4" name="value4" placeholder="Contato" style="width: 450px; height: 45px; font-size: 25px;" required>
                                </div>
                                <div  class="control-group">
                                    <!--[if lt IE 10]><label class="control-label" for="new-name">Endereço:</label><![endif]-->
                                    <select id="contact_type5" name="contact_type5" style="height: 55px; font-size: 25px">
                                        <option>Telefone Fixo</option>
                                        <option>Telefone Celular</option>
                                        <option>Nextel</option>
                                        <option>Email</option>
                                        <option selected="selected">WebSite</option>
                                    </select>
                                    <input type="text" id="value5" name="value5" placeholder="Contato" style="width: 450px; height: 45px; font-size: 25px;" required>
                                </div>
                        </div>
                        <div id="step-4" style="height: 450px">
                            <h2 class="StepTitle">Preencha o seu anúncio</h2>
                            <br />

                            <div class="input-append control-group">
                                <!--[if lt IE 10]><label class="control-label" for="new-name">Empresa:</label><![endif]-->
                                <input type="text" id="advertising_title" name="advertising_title" placeholder="Título do anúncio Ex.: Desenvolvimento de Sites" style="width: 672px; height: 45px; font-size: 25px;" required>
                                <button class="btn btn-info announcement-tooltip" type="button" data-toggle="popover" data-placement="bottom" data-content="Digite aqui o título do seu anúncio. ..............." title="" style="height: 55px;">?</button>
                            </div>
                            <br>
                            <div class="input-append control-group">
                                <!--[if lt IE 10]><label class="control-label" for="new-name">Empresa:</label><![endif]-->
                                <textarea id="advertising_content" name="advertising_content" placeholder="Conteúdo do anúncio Ex.: Desenvolvimento de Sites, site expresso em até dois dias. Bla bla bla" style="width: 672px; height: 290px; font-size: 25px;" required></textarea>
                                <button class="btn btn-info announcement-tooltip" type="button" data-toggle="popover" data-placement="bottom" data-content="Digite aqui o conteúdo do seu anúncio. ..............." title="" style="height: 300px;">?</button>
                            </div>

                        </div>
                        <div id="step-5" style="height: 450px">
                            <h2 class="StepTitle">Prévia do seu anúncio</h2>
                            <br />
                            Seu anúncio irá aparecer assim.
                            <br />
                            <div class="ResultadoBuscaAnuncio">
                                <h2><span id="anuncio_titulo" class="anuncio_titulo">Título Anuncio</span> - <span class="empresa_nome">Nome da Empresa</span></h2>
                                <div>
                                    <span id="anuncio_conteudo" class="anuncio_conteudo">Conteúdo do anúncio</span>
                                </div>
                                <div>
                                    <address>
                                        <span>Endereço:</span> <var class="endereco">Endereço</var> nº <var class="endereco_numero">Número</var> - <var class="endereco_bairro">Bairro</var> - <var class="endereco_cidade">Cidade</var> - <var class="endereco_estado">Estado</var>
                                    </address>
                                </div>
                                <div class="anuncioTelefone">
                                    Carregando... <img src="/public/site/img/294.gif" style="vertical-align: text-bottom" />
                                </div>
                                <div class="pull-right">
                                    <ul class="unstyled inline">
                                        <li><div class="btn btn-warning btn-contacts">Ver Telefone</div></li>
                                        <li><a href="#" class="btn btn-warning">+ Detalhes</a></li>
                                        <li><a href="#" class="btn btn-warning">Ligue para mim</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="step-6" style="height: 450px">
                            <h2 class="StepTitle">Confirme seus dados</h2>
                            <br />
                            <fieldset>
                                <legend>Dados da Empresa</legend>
                                Nome: <span class="empresa_nome"></span><br />
                                Categoria: <span class="empresa_categoria"></span><br />
                                <!--Serviço(s): <script>$('#ms1').getValue()</script><br /-->
                            </fieldset>

                            <fieldset>
                                <legend>Endereço</legend>
                                <div><span>Endereço:</span> <var class="endereco">Endereço</var> nº <var class="endereco_numero">Número</var> - <var class="endereco_bairro">Bairro</var> - <var class="endereco_cidade">Cidade</var> - <var class="endereco_estado">Estado</var></div>
                            </fieldset>

                            <fieldset>
                                <legend>Contatos</legend>
                                <div><!--Contato 1:--></div><var class="contato_tipo1"></var>: <var class="contato1"></var><br />
                                <div><!--Contato 2:--></div><var class="contato_tipo2"></var>: <var class="contato2"></var><br />
                                <div><!--Contato 3:--></div><var class="contato_tipo3"></var>: <var class="contato3"></var><br />
                                <div><!--Contato 4:--></div><var class="contato_tipo4"></var>: <var class="contato4"></var><br />
                                <div><!--Contato 5:--></div><var class="contato_tipo5"></var>: <var class="contato5"></var><br />
                            </fieldset>

                        </div>
                    </div>
                <!-- End SmartWizard Content -->
                <!--Modal-->
            </div>
        </div>
        <input id="category_name_text" name="category_name_text" type="hidden" value="" />
        <input id="state_name_text" name="state_name_text" type="hidden" value="" />
        <input id="city_name_text" name="city_name_text" type="hidden" value="" />
    </form>
</div>
{if $smarty.get.cadastrogratis|default: ''}
<script type="text/javascript">
    $(document).ready(function() {
        $('#ModalCadastroGratis').modal();
    });
</script>
<div id="ModalCadastroGratis" class="modal fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Anúncio Grátis - Guia da Baixada</h3>
    </div>
    <div class="modal-body">
        <p>Cadastro efetuado com sucesso. Em breve iremos analizar seu pedido e cadastra-lo em nosso sistema. Obrigado!</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Fechar</a>
    </div>
</div>
{/if}