                                            <div class="span4">
                                                <!-- Aside -->
                                                <aside>
                                                    {*<div class="widget">
                                                        <!-- Social Counter Widget -->
                                                        <div class="social-counter">
                                                            <div class="row-fluid">
                                                                <div class="span4">
                                                                    <div class="twitter-followers">
                                                                        <a href="https://twitter.com/h1internet" data-toggle="tooltip" title="Siga-nos no Twitter" target="_blank">
                                                                            <i class="icon-twitter"></i>
                                                                            <span class="title">Twitter</span>
                                                                            <span id="twitter-followers"></span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="span4">
                                                                    <div class="facebook-likes">
                                                                        <a href="http://www.facebook.com/CatalogodaRegiao" data-toggle="tooltip" title="Acessar Facebook" target="_blank">
                                                                            <i class="icon-facebook"></i>
                                                                            <span class="title">Facebook</span>
                                                                            <span id="facebook-likes"></span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Social Counter Widget -->
                                                    </div>*}
                                                    
                                                    <div class="widget">
                                                        <!-- Newsletter Widget -->
                                                        <div class="newsletter newsletter-widget">
                                                            <div class="row-fluid">
                                                                    <p>Cadastre seu e-mail <strong>receba informações exclusivas</strong> do Guia da Baixada.</p>
                                                                    <form class="form-horizontal"action="#" method="post">
                                                                        <div class="control-group">
                                                                            <div class="input-prepend">
                                                                                <span class="add-on"><i class="icon-user"></i></span>
                                                                                <input style="width: 251px" type="text" id="contact_custom_fields_nome" name="contact[custom_fields][nome]" placeholder="Digite seu name"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <div class="input-prepend input-append">
                                                                                <span class="add-on"><i class="icon-envelope"></i></span>
                                                                                <input style="width: 163px" type="text" id="email" name="contact[email]" placeholder="Digite seu e-mail"/>
                                                                                <button class="btn" type="submit">Cadastrar</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    {*<div id="opt_5214b04338a582b3a70004f6" style="height: 240px"></div>*}
                                                                    <small><i class="icon-warning-sign"></i> Seu e-mail não será divulgado.</small>
                                                            </div>
                                                        </div>
                                                        <!-- End Newsletter Widget -->
                                                    </div>

                                                    {*<div class="widget">
                                                        <h2><i class="icon-building"></i> <span>Ache nos</span> Estados</h2>
                                                        <!-- Cities Widget -->
                                                        <div class="cities cities-widget">
                                                            <div class="row">
                                                            {foreach $state_list as $state}
                                                                {if $state@first}
                                                                <div class="span1">
                                                                    <ul class="unstyled">
                                                                {/if}

                                                                {if $state@iteration is div by 7}
                                                                        <li>
                                                                            <a href="/{$state.friendly_name}"><i class="icon-map-marker"></i> {$state.acronym}</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="span1">
                                                                    <ul class="unstyled">
                                                                    {else}
                                                                        <li>
                                                                            <a href="/{$state.friendly_name}"><i class="icon-map-marker"></i> {$state.acronym}</a>
                                                                        </li>
                                                                    {/if}
                                                                
                                                                {if $state@last}
                                                                    </ul>
                                                                </div>
                                                                {/if}
                                                            {/foreach}
                                                            </div>
                                                        </div>
                                                        <!-- End Cities Widget -->
                                                    </div>*}

                                                    {*<div class="widget">
                                                        <h2><i class="icon-twitter"></i> <span>Últimos</span> Tweets</h2>
                                                        <!-- Twitter Widget -->
                                                        <div class="twitter-widget">
                                                            <a class="twitter-timeline" href="https://twitter.com/catalogoregiao" data-widget-id="361870869779329026">Tweets de @catalogoregiao</a>
                                                            <script>!function(d,s,id) { var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)) { js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                                        </div>
                                                        <!-- End Twitter Widget -->
                                                    </div>*}
                                                </aside>
                                                <!-- End Aside -->
                                            </div>