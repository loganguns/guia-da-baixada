<div class="alert alert-block alert-error fade in">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4 class="alert-titleing">Ooops!</h4>
    <p>Ocorreu um erro inesperado! Um relatório de erro foi gerado e enviado a nossa
        equipe de desenvolvedores e em breve solucionaremos este problema.
        <br />Desculpe o incômodo.</p>
</div>
