{include "dashboard/common/header.tpl"}
<h2 class="glyphicons list"><i></i> Categorias</h2>
<div class="separator"></div>
	
<form class="form-horizontal" style="padding-top: 10px; margin-bottom: 0;" id="validateSubmitForm" method="post" action="/painel/categoria/acao/editar/id/{$category_info.idcategory|default:""}" autocomplete="off">
	
	<div class="row-fluid">
            <div class="span8">
                    <div class="well" style="padding-bottom: 10px;">
                            <h4>Edição de Categoria</h4>
                            <hr class="separator" />
                            <div class="row-fluid">
                                <div class="span12">
                                        <div class="control-group">
                                                <label class="control-label" for="category_name"> Nome</label>
                                                <div class="controls"><input class="span12" id="category_name" name="category_name" type="text" value="{$category_info.category_name|default:""}" /></div>
                                        </div>
                                        <div class="control-group">
                                                <label class="control-label" for="category_description"> Descrição</label>
                                                <div class="controls"><textarea class="span12" id="category_description" name="category_description">{$category_info.description|default:""}</textarea></div>
                                        </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                    <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Salvar</button>
                                    <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancelar</button>
                            </div>
                    </div>
            </div>
            
	</div>
        <div class="row-fluid">
            <div class="span12">
                        <div class="well" style="padding-bottom: 10px;">
                                <h4>anuncios Cadastrados</h4>
                                <hr class="separator" />
                                <div class="row-fluid">
                                    <div class="span12">
                                        <table cellpadding="0" cellspacing="0" border="0" class="dynamicTable table table-striped table-bordered table-primary table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nome</th>
                                                    <th>Descrição</th>
                                                    <th>Opções</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $category_list as $cat}
                                                <tr class="gradeX">
                                                    <td>{$cat.idcategory}</td>
                                                    <td>{$cat.category_name}</td>
                                                    <td>{$cat.description}</td>
                                                    <td class="center">
                                                        <a href="http://{$smarty.server.SERVER_NAME}/painel/categoria/acao/editar/id/{$cat.idcategory}" class="btn-action glyphicons pencil btn-success"><i></i></a>
                                                        <a href="#" class="btn-action glyphicons remove_2 btn-danger"><i></i></a>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
        </div>
</form>
{include "dashboard/common/footer.tpl"}
