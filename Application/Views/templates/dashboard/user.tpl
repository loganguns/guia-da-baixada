{include "dashboard/common/header.tpl"}
<h2 class="glyphicons user"><i></i> Usuários</h2>
<div class="separator"></div>
	
        <div class="row-fluid">
            <div class="span12">
                        <div class="well" style="padding-bottom: 10px;">
                                <h4>Usuários Cadastrados</h4>
                                <hr class="separator" />
                                <div class="row-fluid">
                                    <div class="span12">
                                        <table cellpadding="0" cellspacing="0" border="0" class="dynamicTable table table-condensed table-primary table-vertical-center table-thead-simple">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nome</th>
                                                    <th>Email</th>
                                                    <th>Opções</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $user_list as $usr}
                                                <tr class="gradeX">
                                                    <td>{$usr.iduser}</td>
                                                    <td>{$usr.name}</td>
                                                    <td><a href="emailto:{$usr.email}">{$usr.email}</a></td>
                                                    <td class="center">
                                                        <a href="#" class="btn-action glyphicons pencil btn-success"><i></i></a>
                                                        <a href="#" class="btn-action glyphicons remove_2 btn-danger"><i></i></a>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
        </div>
{include "dashboard/common/footer.tpl"}