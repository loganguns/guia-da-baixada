{include "dashboard/common/header.tpl"}
<h2 class="glyphicons notes"><i></i> Anúncios</h2>
<div class="separator"></div>
	
<form class="form-horizontal" style="padding-top: 10px; margin-bottom: 0;" id="validateSubmitForm" method="post" action="/painel/anuncio/acao/novo" autocomplete="off">
	
	<div class="row-fluid">
            <div class="span8">
                    <div class="well" style="padding-bottom: 10px;">
                            <h4>Cadastro de Anúncio</h4>
                            <hr class="separator" />
                            <div class="row-fluid">
                                <div class="span12">
                                        <div class="control-group">
                                            <div class="controls">
                                                <label class="radio">
                                                    <input type="radio" name="advertising_type" value="1">
                                                    Anúncio Grátis
                                                </label>
                                                <br />
                                                <label class="radio">
                                                    <input type="radio" name="advertising_type" value="10">
                                                    Iniciativa H1 Internet
                                                </label>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                                <label class="control-label" for="identerprise"> Empresa</label>
                                                <div class="controls">
                                                    <select class="span12" id="identerprise" name="identerprise">
                                                        <option value="">Selecione a Empresa</option>
                                                        {foreach $enterprise_list as $ent}
                                                        <option value="{$ent.identerprise}">{$ent.name}</option> 
                                                        {/foreach}
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="control-group">
                                                <label class="control-label" for="advertising_title"> Título</label>
                                                <div class="controls"><input class="span12" id="advertising_title" name="advertising_title" type="text" /></div>
                                        </div>
                                        <div class="control-group">
                                                <label class="control-label" for="advertising_content"> Conteúdo anuncio</label>
                                                <div class="controls"><textarea class="span12" id="advertising_content" name="advertising_content"></textarea></div>
                                        </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                    <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Salvar</button>
                                    <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancelar</button>
                            </div>
                    </div>
            </div>
            
	</div>
        <div class="row-fluid">
            <div class="span12">
                        <div class="well" style="padding-bottom: 10px;">
                                <h4>anuncios Cadastrados</h4>
                                <hr class="separator" />
                                <div class="row-fluid">
                                    <div class="span12">
                                        <table cellpadding="0" cellspacing="0" border="0" class="dynamicTable table table-condensed table-primary table-vertical-center table-thead-simple">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Titulo</th>
                                                    <th>Conteúdo</th>
                                                    <th>Opções</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $advertising_list as $ad}
                                                <tr class="gradeX">
                                                    <td>{$ad.idadvertising}</td>
                                                    <td>{$ad.title}</td>
                                                    <td>{$ad.content}</td>
                                                    <td class="center">
                                                        <a href="/painel/anuncio/acao/editar/id/{$ad.idadvertising}" class="btn-action glyphicons pencil btn-success"><i></i></a>
                                                        <a href="#" class="btn-action glyphicons remove_2 btn-danger"><i></i></a>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
        </div>
</form>
{include "dashboard/common/footer.tpl"}
