{include "dashboard/common/header.tpl"}
<link rel="stylesheet" href="http://{$smarty.server.SERVER_NAME}/public/dashboard/styles/token-input-facebook.css" type="text/css" />
{literal}
<script>
$(function(){
    $('#address_state').change(function(){
        if( $(this).val() ) {
            $('#address_city').hide();
            $('.carregando').show();
            $.getJSON('http://{/literal}{$smarty.server.SERVER_NAME}{literal}/painel/ajax/acao/listcity',{state: $(this).val()}, function(j){
                var options = '<option value="">Selecione</option>';	
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].idcity + '">' + j[i].city_name + '</option>';
                }	
                $('#address_city').html(options).show();
                $('.carregando').hide();
            });
        } else {
            $('#address_city').html('<option value="0">-- Escolha um estado --</option>');
        }
    });
});
</script>
{/literal}

<h2 class="glyphicons cargo"><i></i> Empresas</h2>
<div class="separator"></div>
	
<form class="form-horizontal" style="padding-top: 10px; margin-bottom: 0;" id="validateSubmitForm" method="post" action="/painel/empresa/acao/editar/id/{$enterprise_info.identerprise|default:""}" autocomplete="off">
	
	<div class="row-fluid">
            <div class="span8">
                    <div class="well" style="padding-bottom: 10px;">
                            <h4>Edição de Empresa</h4>
                            <hr class="separator" />
                            <div class="row-fluid">
                                <div class="span8">
                                        <div class="control-group">
                                                <label class="control-label" for="enterprise_name"> Nome da Empresa</label>
                                                <div class="controls"><input class="span12" id="enterprise_name" name="enterprise_name" type="text" value="{$enterprise_info.name|default:""}"/></div>
                                        </div>
                                        <div class="control-group">
                                                <label class="control-label" for="enterprise_description"> Descrição</label>
                                                <div class="controls"><textarea class="span12" id="enterprise_description" name="enterprise_description">{$enterprise_info.description|default:""}</textarea></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="service_arr"> Serviços</label>
                                            <div class="controls">
                                                <input type="text" id="service_arr" name="service_arr" class="span12"/>
                                                <script type="text/javascript">
                                                    $(document).ready(function() {
                                                        $("#service_arr").tokenInput("http://{$smarty.server.SERVER_NAME}/painel/ajax/acao/listService", {
                                                            preventDuplicates: true,
                                                            theme: "facebook",
                                                            prePopulate: [
                                                            {foreach $service_assoc_list as $sub}
                                                                { id: {$sub.idservice}, service_name: "{$sub.service_name}" },
                                                            {/foreach}
                                                                ]
                                                        });
                                                    });
                                                </script>   
                                            </div>
                                        </div>
                                </div>
                            </div>
                                                    
                            <h4>Endereço</h4>
                            <hr class="separator" />
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <label class="control-label" for="address_address"> Endereço</label>
                                        <div class="controls"><input class="span12" id="address_address" name="address_address" type="text" value="{$enterprise_info.address|default:""}" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address_number"> Número</label>
                                        <div class="controls"><input class="span12" id="address_number" name="address_number" type="text" value="{$enterprise_info.number|default:""}" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address_complement"> Complemento</label>
                                        <div class="controls"><input class="span12" id="address_complement" name="address_complement" type="text" value="{$enterprise_info.complement|default:""}" /></div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="type_address_idtype_address"> Tipo</label>
                                        <div class="controls">
                                            <select class="span12" id="type_address_idtype_address" name="type_address_idtype_address">
                                                <option value="1">Residêncial</option>
                                                <option value="2" selected="selected">Comercial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address_state">Estado</label>
                                        <div class="controls">
                                            <select class="span12" id="address_state" name="address_state">
                                                <option value="0">Selecione</option>
                                                {foreach $state_list as $state}
                                                <option value="{$state.idstate}" {if $state.idstate == $enterprise_info.state_idstate}selected="selected"{/if}>{$state.state_name}</option>
                                                {/foreach}
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address_city">Cidade</label>
                                        <div class="controls">
                                            {if $state.idstate > 0}
                                            <script>
                                           $(function(){
                                               $('#address_city').hide();
                                               $('.carregando').show();
                                               $.getJSON('http://{$smarty.server.SERVER_NAME}/painel/ajax/acao/listcity',{ state: '{$enterprise_info.state_idstate}' }, function(j){
                                                       var options = '<option value="0">Selecione</option>';
                                                       for (var i = 0; i < j.length; i++) {
                                                           if(j[i].idcity === '{$enterprise_info.city_idcity}'){
                                                               options += '<option value="' + j[i].idcity + '"  selected="selected">' + j[i].city_name + '</option>';
                                                           }else{
                                                               options += '<option value="' + j[i].idcity + '">' + j[i].city_name + '</option>';
                                                           }
                                                       }	
                                                       $('#address_city').html(options).show();
                                                       $('.carregando').hide();
                                               });
                                           });
                                           </script>
                                           {/if}
                                        <span class="carregando" style="display: none;">Aguarde, carregando...</span>
                                            <select class="span12" name="address_city" id="address_city">
                                                    <option value="0">-- Escolha um estado --</option>
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="district_iddistrict"> Bairro</label>
                                        <div class="controls">
                                            <select class="span12" id="district_iddistrict" name="district_iddistrict">
                                                <option value="1">Centro</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                    <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Salvar</button>
                                    <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancelar</button>
                            </div>
                    </div>
            </div>
	</div>                                
        <div class="row-fluid">
            <div class="span12">
                        <div class="well" style="padding-bottom: 10px;">
                                <h4>Empresas Cadastradas</h4>
                                <hr class="separator" />
                                <div class="row-fluid">
                                    <div class="span12">
                                        <table cellpadding="0" cellspacing="0" border="0" class="dynamicTable table table-condensed table-primary table-vertical-center table-thead-simple">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nome</th>
                                                    <th>Descrição</th>
                                                    <th>Opções</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $enterprise_list as $ent}
                                                <tr class="gradeX">
                                                    <td>{$ent.identerprise}</td>
                                                    <td>{$ent.name}</td>
                                                    <td>{$ent.description}</td>
                                                    <td class="center">
                                                        <a href="http://{$smarty.server.SERVER_NAME}/painel/empresa/acao/editar/id/{$ent.identerprise}" class="btn-action glyphicons pencil btn-success"><i></i></a>
                                                        <a href="#" class="btn-action glyphicons remove_2 btn-danger"><i></i></a>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
        </div>
</form>
{include "dashboard/common/footer.tpl"}
