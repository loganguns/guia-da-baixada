{include "dashboard/common/header.tpl"}
<h2 class="glyphicons cogwheel"><i></i> Serviços</h2>
<div class="separator"></div>
	
<form class="form-horizontal" style="padding-top: 10px; margin-bottom: 0;" id="validateSubmitForm" method="post" action="/painel/servico/acao/editar/id/{$service_info.idservice|default:""}" autocomplete="off">
	
	<div class="row-fluid">
            <div class="span8">
                    <div class="well" style="padding-bottom: 10px;">
                            <h4>Edição de Serviços</h4>
                            <hr class="separator" />
                            <div class="row-fluid">
                                <div class="span12">
                                        <div class="control-group">
                                                <label class="control-label" for="service_name"> Nome</label>
                                                <div class="controls"><input class="span12" id="service_name" name="service_name" type="text" value="{$service_info.service_name|default:""}" /></div>
                                        </div>
                                        <div class="control-group">
                                                <label class="control-label" for="category_idcategory"> Categoria</label>
                                                <div class="controls">
                                                    <select class="span12" id="category_idcategory" name="category_idcategory" type="text">
                                                        <option value="">Selecione a Categoria</option>
                                                        {foreach $category_list as $cat}
                                                        <option value="{$cat.idcategory}"{if $cat.idcategory == $service_info.category_idcategory} selected="selected"{/if}>{$cat.category_name}</option> 
                                                        {/foreach}
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="control-group">
                                                <label class="control-label" for="service_description"> Descrição</label>
                                                <div class="controls"><textarea class="span12" id="service_description" name="service_description">{$service_info.description|default:""}</textarea></div>
                                        </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                    <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Salvar</button>
                                    <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancelar</button>
                            </div>
                    </div>
            </div>
	</div>
        <div class="row-fluid">
            <div class="span12">
                        <div class="well" style="padding-bottom: 10px;">
                                <h4>Serviços Cadastrados</h4>
                                <hr class="separator" />
                                <div class="row-fluid">
                                    <div class="span12">
                                        <table cellpadding="0" cellspacing="0" border="0" class="dynamicTable table table-condensed table-primary table-vertical-center table-thead-simple">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nome</th>
                                                    <th>Descrição</th>
                                                    <th>Opções</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $service_list as $sub}
                                                <tr class="gradeX">
                                                    <td>{$sub.idservice}</td>
                                                    <td>{$sub.service_name}</td>
                                                    <td>{$sub.description}</td>
                                                    <td class="center">
                                                        <a href="http://{$smarty.server.SERVER_NAME}/painel/servico/acao/editar/id/{$sub.idservice}" class="btn-action glyphicons pencil btn-success"><i></i></a>
                                                        <a href="#" class="btn-action glyphicons remove_2 btn-danger"><i></i></a>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
        </div>
</form>
{include "dashboard/common/footer.tpl"}
