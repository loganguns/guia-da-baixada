<?php
/**
* ErrorController - The default error controller class
*
* File: application/controllers/ErrorController.php
*
*/

require_once 'Zend/Controller/Action.php' ;
require_once 'Zend/Log.php';

class ErrorController extends Zend_Controller_Action 
{

    /**
     * This action handles
     *    - Application errors
     *    - Errors in the controller chain arising from missing
     *     controller classes and/or action methods
     */
    public function errorAction()
    {
        $view = Zend_Registry::get('smarty');
      	//$title = Zend_Registry::get('title');
      	//$view->assign('title',$title);
        $errors = $this->_getParam ('error_handler') ;
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER :
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION :
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $error = '404';
                break ;
            default :
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $error = '500';
                break ;
        }
        
        // Log the exception
            $exception = $errors->exception;
            $log =  new Zend_Log(
                    new Zend_Log_Writer_Stream('../logs/error.log' )
                    );
            $log->debug(
             $exception->getMessage() . PHP_EOL . $exception->getTraceAsString()
            );
            
        // show error page
        $view->assign('error',$error);
        $view->assign('errorDetail', $exception);
        $view->render('error.tpl');
    }
}