<?php
/** Zend_Controller_Action */
require_once 'BaseController.php';
require_once 'Announcement.php';
        
class SearchController extends Base_Controller
{
    public function init(){
        
    }
    
    public function indexAction(){
        $this->getRequest()->getParam("searchTerm");
        $view = Zend_Registry::get('smarty');
        $term = $this->getRequest()->getParam("searchTerm");
        $local = $this->getRequest()->getParam("local");
        $view->assign('announcements', Announcement::search('teste', 'araruama'));
        $view->render('site/search_result.tpl');
    }
}