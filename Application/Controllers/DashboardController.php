<?php

/**
 * Description of dashboardController
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

/** Zend_Controller_Action */
require_once 'BaseController.php';
Zend_Loader::loadClass("Zend_Controller_Action");
Zend_Loader::loadClass("User");
Zend_Loader::loadClass("Category");
Zend_Loader::loadClass("Service");
Zend_Loader::loadClass("Contact");
Zend_Loader::loadClass("Enterprise");
Zend_Loader::loadClass("Advertising");
Zend_Loader::loadClass("Address");
Zend_Loader::loadClass("Zend_Json");
Zend_Loader::loadClass("Enterprisehasservice");

class DashboardController extends Base_Controller{
    
    public function init(){
        
        $front = Zend_Controller_Front::getInstance()->getRequest();
        
        $view = Zend_Registry::get('smarty');
        $view->assign('action', $front->action);
    }

    public function indexAction(){
        
      $view = Zend_Registry::get('smarty');
      $view->render('dashboard/dashboard.tpl');
    }
    
    public function loginAction(){
        
        $view = Zend_Registry::get('smarty');
        
        $htt_request = new Zend_Controller_Request_Http;
        
        if($htt_request->isPost()){
            if($this->getRequest()->getParam("email") == null ||
                    $this->getRequest()->getParam("password") == null){
             $view->assign('status',1); //Login ou Senha em branco
             $this->_redirect('dashboard/?');
            }
            else{
                $User = new User($iduser = null,
                        $email = $this->getRequest()->getParam("email"),
                        $password = $this->getRequest()->getParam("password"),
                        $name = null,
                        $status = null,
                        $deleted = null,
                        $user_type_iduser_type = null,
                        $address_idaddress = null,
                        $activateCode = null);
                $User->login();
                if($User->getStatus()){   
                    $auth = new Zend_Session_Namespace('loginUser');
                    $auth->login = true;
                    $auth->iduser = $User->getIduser();
                    $auth->name = $User->getName();
                    $auth->password = $User->getPassword();
                    $auth->email = $User->getEmail();
                    $auth->userInfo = $User->getUserInfo();
                    $auth->userType = $User->getUser_type_iduser_type();
                    $this->_redirect('dashboard/categoria/');
                }else{
                    $view->assign('status',2); //Login ou Senha incorretos
                    $view->render('dashboard/login.tpl');
                    //$this->_redirect('dashboard/');
                }
            }
        }else{
            $view->assign('status',2); //Login ou Senha incorretos
            $view->render('dashboard/login.tpl');
            //$this->_redirect('dashboard/');
        }
    }
    
    public function logoffAction(){
        
        $view = Zend_Registry::get('smarty');
        $login_status = new Zend_Session_Namespace('loginUser');
        if($login_status->login){
        unset($login_status->login);
        unset($login_status->iduser);
        unset($login_status->name);
        unset($login_status->password);
        unset($login_status->email);
        $view->assign('status',4);
        $this->_redirect('/dashboard/');
      }
      else{
        $this->_redirect('/dashboard/');
      }
    }
    
    public function categoriaAction(){
        
        $view = Zend_Registry::get('smarty');
        
        $view->assign('category_list',Category::_list());
        
        $acao = $this->getRequest()->getParam("acao");
        
        if ($acao){
            
            switch($acao){
                
                case 'novo';
                    if($this->getRequest()->getParam("category_name") != ""){
                        $category = new Category($this->getRequest()->getParam("category_name"), $this->getRequest()->getParam("category_description"));
                        $category->create();
                        $view->assign('category_list',Category::_list());
                    }
                    $view->render('dashboard/category_create.tpl');
                    
                    break;
                    
                case 'editar';
                    
                    $htt_request = new Zend_Controller_Request_Http;
                    if($htt_request->isPost()){
                        
                        $Category = new Category(
                                $name = $this->getRequest()->getParam("category_name"),
                                $description = $this->getRequest()->getParam("category_description"),
                                $idcategory = $this->getRequest()->getParam("id"));
                        $Category->edit();
                        
                        $view->assign('category_list',Category::_list());
                    }
        
                    $view->assign('category_info',  Category::search($this->getRequest()->getParam("id")));
                    $view->render('dashboard/category_edit.tpl');
                    
                    break;
            }
        }
        else{
            $view->assign('category_list',Category::_list());
        
            $view->render('dashboard/category_create.tpl');
        }
    }
    
    public function servicoAction(){
        
        $view = Zend_Registry::get('smarty');
        $view->assign('category_list',Category::_list());
        $view->assign('service_list',Service::_list());
        
        $acao = $this->getRequest()->getParam("acao");
        
        if ($acao){
            
            switch($acao){
                
                case 'novo';
                    if($this->getRequest()->getParam("service_name") != ""){
                        //$category = new Service(
                        //        $this->getRequest()->getParam("category_idcategory"),
                        //        $this->getRequest()->getParam("service_name"),
                        //        $this->getRequest()->getParam("service_description"));
                        //$category->create();
                        $subname = explode(";", $this->getRequest()->getParam("service_name"));
                        //var_dump($subname);
                        $count = count($subname);
                        for($i=0;$i<$count;$i++){
                            $Service = new Service(
                                $this->getRequest()->getParam("category_idcategory"),
                                $subname[$i],
                                $this->getRequest()->getParam("service_description"));
                            $Service->create();
                            $view->assign('service_list',Service::_list());
                        }
                    }
                    
                    $view->render('dashboard/service_create.tpl');
                    
                    break;
                    
                case 'editar';
                    
                    $htt_request = new Zend_Controller_Request_Http;
                    if($htt_request->isPost()){
                        $Service = new Service(
                                $category_idcategory = $this->getRequest()->getParam("category_idcategory"),
                                $service_name = $this->getRequest()->getParam("service_name"),
                                $description = $this->getRequest()->getParam("service_description"),
                                $idservice = $this->getRequest()->getParam("id"));
                        $Service->edit();
                        $view->assign('service_list',Service::_list());
                    }
                    
                    $view->assign('service_info',Service::search($this->getRequest()->getParam("id")));
                    $view->render('dashboard/service_edit.tpl');
                    
                    break;
            }
        }
        else{
            $view->render('dashboard/service_create.tpl');
        }
    }
    
    public function empresaAction(){
        
        $view = Zend_Registry::get('smarty');
        $view->assign('enterprise_list',  Enterprise::search());
        $view->assign('service_list',Service::_list());
        $view->assign('state_list', Address::listState());
        $view->assign('type_list', Contact::listType());
        
        $acao = $this->getRequest()->getParam("acao");
        
        if ($acao){
            
            switch($acao){
                
                case 'novo';
                    if($this->getRequest()->getParam("enterprise_name") != ""){
                        
                        $login = new Zend_Session_Namespace('loginUser');
                        
                        $Address = new Address(
                                $address = $this->getRequest()->getParam("address_address"),
                                $number = $this->getRequest()->getParam("address_number"),
                                $complement = $this->getRequest()->getParam("address_complement"),
                                $status = 1,
                                $latitute = null,
                                $longitude = null,
                                $type_address_idtype_address = $this->getRequest()->getParam("type_address_idtype_address"),
                                $state_idstate = $this->getRequest()->getParam("address_state"),
                                $city_idcity = $this->getRequest()->getParam("address_city"),
                                $district_iddistrict = $this->getRequest()->getParam("district_iddistrict"),
                                $country_idcountry = 30);
                        $Address->create();
                        
                        $Enterprise = new Enterprise(
                                $identerprise = null,
                                $enterprise_name = $this->getRequest()->getParam("enterprise_name"),
                                $description = $this->getRequest()->getParam("enterprise_description"),
                                $franchise_idfranchise = 1,
                                $iduser = $login->iduser,
                                $idaddress = $Address->getIdaddress());
                        $Enterprise->create();
                        //print_r($Enterprise);
                        
                        $service_arr = $this->getRequest()->getParam("service_arr");
                        
                        //var_dump($_POST);
                        if($service_arr != null){
                            $service_arr = explode(',', $service_arr);
                            foreach($service_arr as $sub){
                                $Enterprise_has_service = new Enterprisehasservice(
                                        $Enterprise->getIdenterprise(),
                                        $sub);
                                $Enterprise_has_service->create();
                            }
                        }
                        $contactQnt = count($this->getRequest()->getParam("contact_value"));
                        for($i=0; $i<$contactQnt;$i++){
                            $contact_value = $this->getRequest()->getParam("contact_value");
                            $contact_type = $this->getRequest()->getParam("contact_type");
                            if(!empty($contact_value[$i]) || !empty($contact_type[$i])){
                                
                                $Contact = new Contact(
                                        $idcontact = null,
                                        $name = null,
                                        $status = 1,
                                        $value = $contact_value[$i],
                                        $contact_type_idcontact_type = $contact_type[$i],
                                        $identerprise = $Enterprise->getIdenterprise());
                                $Contact->create();
                            }
                        }
                        
                        $Advertising = new Advertising(
                                $idadvertising = null,
                                $title = null,
                                $content = null,
                                $enterprise_identerprise = $Enterprise->getIdenterprise(),
                                $idadvertising_type = 10);
                               
                        $Advertising->create();
                    }
                        
                    $view->assign('enterprise_list',  Enterprise::search());
                    $view->render('dashboard/enterprise_create.tpl');
                    
                    break;
                    
                case 'editar';
                    
                    $htt_request = new Zend_Controller_Request_Http;
                    if($htt_request->isPost()){
                        $userInfo = Zend_Registry::get('userInfo');
                        $enterprise_info =  Enterprise::search($this->getRequest()->getParam("id"));
                        
                        $Address = new Address(
                                $address = $this->getRequest()->getParam("address_address"),
                                $number = $this->getRequest()->getParam("address_number"),
                                $complement = $this->getRequest()->getParam("address_complement"),
                                $status = 1,
                                $latitute = NULL,
                                $longitude = NULL,
                                $type_address_idtype_address = $this->getRequest()->getParam("type_address_idtype_address"),
                                $state_idstate = $this->getRequest()->getParam("address_state"),
                                $city_idcity = $this->getRequest()->getParam("address_city"),
                                $district_iddistrict = $this->getRequest()->getParam("district_iddistrict"),
                                $country_idcountry = 30,
                                $idaddress = $enterprise_info["address_idaddress"]);
                        $Address->edit();
                        
                        
                        $Enterprise = new Enterprise(
                                $identerprise = $this->getRequest()->getParam("id"),
                                $enterprise_name = $this->getRequest()->getParam("enterprise_name"),
                                $description = $this->getRequest()->getParam("enterprise_description"),
                                $franchise_idfranchise = 1,
                                $iduser = $userInfo->iduser,
                                $idaddress = $Address->getIdaddress());
                        $Enterprise->edit();
                        
                        $service_arr = $this->getRequest()->getParam("service_arr");
                        if($this->getRequest()->getParam("service_arr") != null){
                            Enterprisehasservice::delete($Enterprise->getIdenterprise());
                            $service_arr = explode(',', $service_arr);
                            foreach($service_arr as $sub){
                                $Enterprise_has_service = new Enterprisehasservice(
                                        $Enterprise->getIdenterprise(),
                                        $sub);
                                $Enterprise_has_service->create();
                            }
                        }
                        
                        $view->assign('enterprise_list',  Enterprise::search());
                    }
        
                    $view->assign('enterprise_info',  Enterprise::search($this->getRequest()->getParam("id")));
                    $view->assign('service_assoc_list',  Enterprisehasservice::listAssocSub($this->getRequest()->getParam("id")));
                    $view->render('dashboard/enterprise_edit.tpl');
                    
                    break;
            }
        }
        else{
            $view->render('dashboard/enterprise_create.tpl');
        }
    }
    
    public function anuncioAction(){
        $view = Zend_Registry::get('smarty');
        $view->assign('advertising_list', Advertising::search());
        $view->assign('enterprise_list',  Enterprise::search());
        
        $acao = $this->getRequest()->getParam("acao");
        
        if ($acao){
            
            switch($acao){
                
                case 'novo';
                    
                    if($this->getRequest()->getParam("advertising_title") != "" and
                        $this->getRequest()->getParam("advertising_content") != "" ){
                        
                        $user = new Zend_Session_Namespace('loginUser');
                        
                        $Advertising = new Advertising(
                                $idadvertising = null,
                                $title = $this->getRequest()->getParam("advertising_title"),
                                $content = $this->getRequest()->getParam("advertising_content"),
                                $enterprise_identerprise = $this->getRequest()->getParam("identerprise"),
                                $idadvertising_type = $this->getRequest()->getParam("advertising_type"));
                               
                        $Advertising->create();
                        
                    }
                    $view->assign('advertising_list', Advertising::search());
                    $view->render('dashboard/advertising_create.tpl');
                    
                    break;
                    
                case 'editar';
                    
                    $htt_request = new Zend_Controller_Request_Http;
                    if($htt_request->isPost()){
                        
                        $Advertising = new Advertising(
                                $idadvertising = $this->getRequest()->getParam("id"),
                                $title = $this->getRequest()->getParam("advertising_title"),
                                $content = $this->getRequest()->getParam("advertising_content"),
                                $enterprise_identerprise = $this->getRequest()->getParam("identerprise"),
                                $idadvertising_type = null);
                        $Advertising->edit();
                    }
                    $view->assign('advertising_list', Advertising::search());
                    $view->assign('advertising_info',  Advertising::search($this->getRequest()->getParam("id")));
                    $view->render('dashboard/advertising_edit.tpl');
                    
                    break;
            }
        }
        else{
            $view->render('dashboard/advertising_create.tpl');
        }
    }
    
    public function usuariosAction(){
        $view = Zend_Registry::get('smarty');
        $view->assign('user_list', User::getList());
        
        $view->render('dashboard/user.tpl');
    }
    
    public function ajaxAction(){
        
        switch($this->getRequest()->getParam("acao")){
            
            case "upload";
                new Upload();
                echo "upload";
                break;
            
            case "listService";
                if($this->getRequest()->getParam("q") != null){
                    $arr = Service::searchByName($this->getRequest()->getParam("q"));
                    
                    $json = Zend_Json::encode($arr);
                    if($this->getRequest()->getParam("callback") !== null) {
                        $json = $this->getRequest()->getParam("callback") . "(" . $json . ")";
                    }
                }else{
                    $arr = Service::_list();
                    $json = Zend_Json::encode($arr);
                }
                $this->getResponse()->setHeader('Content-type', 'application/json');
                $this->getResponse()->clearBody();
                $this->getResponse()->setBody($json);
                break;
                
            case "deletar";
                $user = new user();
                $user->delete($this->getRequest()->getParam("id"));
                $this->_redirect('/admin/usuarios');

                break;
                
            case "listcity";
                $city = Address::listcity($this->getRequest()->getParam("state"));
                $json = Zend_Json::encode($city);
                //$this->getResponse()->setHeader('Content-type', 'application/json');
                $this->getResponse()->clearBody();
                $this->getResponse()->setBody($json);
                
                break;
        }
    }
}