<?php

/**
 * Description of AjaxController
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

require_once 'Helper.php';
require_once 'BaseController.php';
Zend_Loader::loadClass('Zend_Json');
Zend_Loader::loadClass('Category');
Zend_Loader::loadClass('Enterprise');
Zend_Loader::loadClass('Service');
Zend_Loader::loadClass('Address');
Zend_Loader::loadClass('Zend_Http_Cookie');

class AjaxController extends Base_Controller {
    
    public function getcontactinfoAction(){
        $enterpriseid = $this->getRequest()->getParam("enterpriseid");
        $content = Enterprise::getContactInfo($enterpriseid);
        $json = Zend_Json::encode($content);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->clearBody();
        $this->getResponse()->setBody($json);
    }
    
    public function getcategorylistAction(){
        $content = Category::_list();
        $json = Zend_Json::encode($content);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->clearBody();
        $this->getResponse()->setBody($json);
    }
    
    public function getservicelistAction(){
        $content = Service::searchByName($this->getRequest()->getParam("q"));
        $json = Zend_Json::encode($content);
        if($this->getRequest()->getParam("callback") !== null) {
            $json = $this->getRequest()->getParam("callback") . "(" . $json . ")";
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->clearBody();
        $this->getResponse()->setBody($json);
    }
    
    public function getservicelistbycategoryidAction(){
        $idcategory = $this->getRequest()->getParam("categoryid");
        $content = Service::searchByCategoryId($idcategory);
        $json = Zend_Json::encode($content);
        if($this->getRequest()->getParam("callback") !== null) {
            $json = $this->getRequest()->getParam("callback") . "(" . $json . ")";
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->clearBody();
        $this->getResponse()->setBody($json);
    }
    
    public function getcitylistAction(){
        $city = Address::listcity($this->getRequest()->getParam("state"));
        $json = Zend_Json::encode($city);
        //$this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->clearBody();
        $this->getResponse()->setBody($json);
    }
    
    public function setgeolocationinfoAction(){
        
        $location = Helper::setGeolocationInfo($this->getRequest()->getParam("latlng"));
        if($location === 2){
            $status = array ('status' => 'changed');
        }else{
            $status = array ('status' => 'not_changed');
        }
        $json = Zend_Json::encode($status);
        $this->getResponse()->clearBody();
        $this->getResponse()->setBody($json);
        
    }
}