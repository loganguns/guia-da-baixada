<?php
/** Zend_Controller_Action */
require_once 'BaseController.php';
require_once 'Announcement.php';
require_once 'Category.php';
require_once 'Mail.php';
require_once 'Helper.php';
        
class PortalController extends Base_Controller
{
    public function init(){
//        $request = $this->getRequest();
//        $latlng = $request->getCookie('latlng', NULL);
//        
//        if($latlng !== null){
//            if($request->getCookie('city') !== NULL
//                    && $request->getCookie('state') !== NULL){
//                Geolocation::setInfo($latlng);
//            }
//        }
        $view = Zend_Registry::get('smarty');
        $view->assign('state_list', Helper::getStateList());
    }
    public function indexAction(){
        
        $view = Zend_Registry::get('smarty');
        $view->assign('categoriaDestaque',Category::_listDestaque());
        $view->render('site/index.tpl');
        
    }
    
    public function searchAction(){
        
        $view = Zend_Registry::get('smarty');
        
        $term = str_replace('-', ' ', $this->getRequest()->getParam("searchTerm"));
        $local = str_replace('-', ' ', $this->getRequest()->getParam("city"));
        $ann = Announcement::search(array('searchTerm' => $term, 'local' => $local));
        //
        if(count($ann) == 1){ 
            $this->_request->setActionName("details");
            $view->assign('announcements', $ann);
            $this->detailsAction();
            
        }
        else {
            //
            $view->assign('announcements', $ann);
            //$view->assign('searchParameters', array('searchTerm' => $term, 'local' => $local));
            //print_r(Announcement::search($term, $local));
            $view->render('site/search_result.tpl');
        }
        
    }
    
    public function sobreAction(){
        
        $view = Zend_Registry::get('smarty');
        $view->render('site/sobre.tpl');
        
    }
    
    public function representantesAction() {
        
        $view = Zend_Registry::get('smarty');
        $view->render('site/representantes.tpl');
        
    }
    
    public function parceirosAction(){
        
        $view = Zend_Registry::get('smarty');
        $view->render('site/parceiros.tpl');
        
    }
    
    public function anunciogratisAction() {
        //$view = Zend_Registry::get('smarty');
        //print_r($_POST);
        $body = '';
        foreach($_POST as $key => $item){
            $body .= $key . ' = ' . $item . "<br>";
        }

        $Mail = new Mail('noreply@catalogodaregiao.com.br',
                    $body,
                    null,
                    'H1 Internet',
                    "Cadastro Anuncio Grátis - Guia da Baixada");
        $Mail->send();
        
        $this->_redirect('/?cadastrogratis=true');
    }
    
    public function detailsAction(){
        
        $view = Zend_Registry::get('smarty');
        $view->assign('categoriaDestaque',Category::_listDestaque());
        $view->render('site/details.tpl');
        //echo "Not Yet Implemented";
        
    }
}