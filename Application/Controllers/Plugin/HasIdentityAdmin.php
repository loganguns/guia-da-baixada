<?php

require_once 'Zend/Controller/Plugin/Abstract.php';
require_once '../libs/Zend/Session.php';
class HasIdentityAdmin extends Zend_Controller_Plugin_Abstract
{
   /*
    Nome do módulo, controlador e ação que o usuário terá acesso caso não esteja logado.
    */
   //const module      = 'admin';
   static $arr_protected_controller  = Array ('admin');
   static $arr_noprotected_action  = Array ('login');
   const redirect_controller      = 'admin';
   const redirect_action      = 'denied';

   public function preDispatch(Zend_Controller_Request_Abstract $request)
   {
      //$module     = $request->getModuleName();
      $controller = $request->getControllerName();
      $action     = $request->getActionName();
      //echo "Plugin!";
      // Verifica se o usuário não está logado
      $login_status = new Zend_Session_Namespace('loginAdmin');
      if(!$login_status->login) {
         // Verifica se a requisição é diferente do permitido
         // Se for diferente rotea para a página de login
         //if($module != self::module && $controller != self::controller && $action != self::action) {
          if(in_array($controller,self::$arr_protected_controller) === true &&
                  in_array($action, self::$arr_noprotected_action) === false) {
            //$request->setModuleName(self::module);
            //$view->assign('msg',"Acesso Negado !");
            $request->setControllerName(self::redirect_controller);
            $request->setActionName(self::redirect_action);
            }
         
      }
   }
}