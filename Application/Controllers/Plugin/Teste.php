<?php

require_once 'Zend/Controller/Plugin/Abstract.php';

class Teste extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request){
        
        $view = Zend_Registry::get('smarty');
        @$view->assign('city', $_COOKIE['city']);
        @$view->assign('state', $_COOKIE['state']);
        
        $rota = array('item1' => $this->getRequest()->getParam("state"),
                 'item2' => $this->getRequest()->getParam("city"),
                 'item3' => $this->getRequest()->getParam("searchTerm"));
        //print_r($rota);
        Zend_Registry::set('rota', $rota);
    }
}