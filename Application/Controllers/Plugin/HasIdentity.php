<?php

require_once 'Zend/Controller/Plugin/Abstract.php';
require_once 'Zend/Session.php';
class HasIdentity extends Zend_Controller_Plugin_Abstract
{
   /*
    Nome do módulo, controlador e ação que o usuário terá acesso caso não esteja logado.
    */
   //const module      = 'admin';
   static $arr_protected_controller  = Array ('painel');
   const redirect_controller      = 'painel';
   const redirect_action      = 'login';
   protected $_auth = null;
   
   public function preDispatch(Zend_Controller_Request_Abstract $request)
   {
       $this->_auth = Zend_Auth::getInstance();
        //$module     = $request->getModuleName();
        $controller = $request->getControllerName();
        $action     = $request->getActionName();
        $view = Zend_Registry::get('smarty');
      //echo "Plugin!";
      // Verifica se o usuário não está logado
      $login_status = new Zend_Session_Namespace('loginUser');
      if(!$this->_auth->hasIdentity()) {
         // Verifica se a requisição é diferente do permitido
         // Se for diferente rotea para a página de login
         //if($module != self::module && $controller != self::controller && $action != self::action) {
          $view->assign('userName', null);
          if(in_array($controller,self::$arr_protected_controller) === true) {
            //$request->setModuleName(self::module);
            //$view->assign('msg',"Acesso Negado !");
            $request->setControllerName(self::redirect_controller);
            $request->setActionName(self::redirect_action);
            if($login_status->userType != 4){
                //die("Acesso Negado!");
            }
        }
         
      }else{
        $view = Zend_Registry::get('smarty');
        $view->assign('userName', $login_status->name);
        Zend_Registry::set('userInfo', $login_status);
        if(in_array($controller,self::$arr_protected_controller) === true) {
            if($login_status->userType != 4){
                //die("Acesso Negado!");
            }
        }
      }
   }
}