<?php

/**
 * Description of AuthController
 *
 * @param Array $credentials
 * @return boolean
 * @author Thiago Moreira <loganguns@gmail.com>
 */

Zend_Loader::loadClass('Auth');
Zend_Loader::loadClass('Zend_Controller_Action');
Zend_Loader::loadClass('Zend_Controller_Request_Http');

class AuthController extends Zend_Controller_Action
{
 
    public function init()
    {
    }
 
    public function indexAction()
    {
        return $this->_helper->redirector('login');
    }
 
    public function loginAction()
    {
        //Verifica se existem dados de POST
        $htt_request = new Zend_Controller_Request_Http;
        if($htt_request->isPost()){
            //Formulário corretamente preenchido?
            if ( $this->getParam('email') !== '' && $this->getParam('password') !== '' ) {
                $email = $this->getParam('email');
                $password = $this->getParam('password');
 
                try {
                    Auth::login($email, $password);
                    //Redireciona para o Controller protegido
                    return $this->_helper->redirector->goToRoute( array('controller' => 'painel'), null, true);
                } catch (Exception $e) {
                    //Dados inválidos
                    //$this->_helper->FlashMessenger($e->getMessage());
                    $this->_redirect('/auth/login');
                }
            } else {
                //Formulário preenchido de forma incorreta
                $form->populate($data);
            }
        }
        else{
            $view = Zend_Registry::get('smarty');
            $view->render('dashboard/login.tpl');
        }
    }
 
    public function logoffAction()
    {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        return $this->_helper->redirector('index');
    }
}
