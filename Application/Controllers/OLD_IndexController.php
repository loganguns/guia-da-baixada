<?php
/** Zend_Controller_Action */
require_once 'BaseController.php';
require_once 'Announcement.php';
require_once 'Mail.php';
        
class IndexController extends Base_Controller
{
    
    public function indexAction(){
        
        $view = Zend_Registry::get('smarty');
        $this->getRequest()->getParam("searchTerm");
        
        $view->render('site/index.tpl');
    }
    
    public function anunciogratisAction() {
        //$view = Zend_Registry::get('smarty');
        
        $body = '';
        foreach($_POST as $key => $item){
            $body .= $key . ' = ' . $item . "<br>";
        }

        $Mail = new Mail('loganguns@gmail.com',
                    $body,
                    null,
                    'H1 Internet',
                    "Cadastro Anuncio Grátis - Guia da Baixada");
        $Mail->send();
        
        //$email_content = print_r($_POST);

        //$view->render('site/index.tpl');
        //$this->_redirect('/?cadastrogratis=true');
    }
}