<?php
/** Zend_Controller_Action */
require_once 'BaseController.php';
Zend_Loader::loadClass('User');
Zend_Loader::loadClass('Mail');
Zend_Loader::loadClass('Zend_Validate_EmailAddress');
/**
 * Description of UserController
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

class UserController extends Base_Controller{
    
    public function signAction(){
        
        $view = Zend_Registry::get('smarty');
        $http_request = new Zend_Controller_Request_Http;
        
        if($http_request->isPost()){
            $validator = new Zend_Validate_EmailAddress();
            if($validator->isValid($this->getRequest()->getParam("new-email"))) {
                if(User::isUniqueEmail($this->getRequest()->getParam("new-email"))){
                    $User = new User(null, //IdUser
                                     $this->getRequest()->getParam("new-email"),
                                     $this->getRequest()->getParam("new-pass2"),
                                     $this->getRequest()->getParam("new-name"),
                                     0, //Status
                                     0, //Deleted
                                     1, //User_Type
                                     '', //Id_Address 
                                     $activateCode = base64_encode(uniqid(rand()))); //ActivateCode
                    $create = $User->create();
                    //$create = false;
                    if($create === true){
                        
                        $body = "<a href='http://www.catalogodaregiao.com.br/user/activate";
                        $body .= "/email/" . $this->getRequest()->getParam("new-email");
                        $body .= "/code/" . $activateCode;
                        $body .= "/' >Link de Ativação</a>";
                        
                        $Mail = new Mail($this->getRequest()->getParam("new-email"),
                                    $body,
                                    null,
                                    $this->getRequest()->getParam("new-name"),
                                    "Confirmação de Cadastro em Guia da Baixada");
                        $Mail->send();
                        $view->assign('create', 'success');
                    }else{
                        $view->assign('create', 'error');
                    }
                }  else {
                    $view->assign('create', 'already');
                    $view->assign('new_email', $this->getRequest()->getParam("new-email"));
                    $view->assign('new_name', $this->getRequest()->getParam("new-name"));
                    // Email já cadastrado.
                }
            }
            else {
                $view->assign('create', 'invalid_mail');
                $view->assign('new_email', $this->getRequest()->getParam("new-email"));
                $view->assign('new_name', $this->getRequest()->getParam("new-name"));
               // Email invalido
            }
        }
        
        $view->render('site/user-create.tpl');
    }

    public function activateAction(){
        
        $updateStatus = User::activate($this->getRequest()->getParam("email"),
                $this->getRequest()->getParam("code"));
        
        if($updateStatus){
            $this->_redirect('/?ativado=true');
        }else{
            $this->_redirect('/?ativado=false');
        }
    }
}