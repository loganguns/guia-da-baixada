<?php

/**
 * Description of Enetrprisehasservice
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

class Enterprisehasservice {
    
    private $enterprise_identerprise;
    private $service_idservice;
    
    public function getEnterprise_identerprise() {
        return $this->enterprise_identerprise;
    }

    public function setEnterprise_identerprise($enterprise_identerprise) {
        $this->enterprise_identerprise = $enterprise_identerprise;
    }

    public function getService_idservice() {
        return $this->service_idservice;
    }

    public function setService_idservice($service_idservice) {
        $this->service_idservice = $service_idservice;
    }

    function __construct($enterprise_identerprise, $service_idservice) {
        $this->enterprise_identerprise = $enterprise_identerprise;
        $this->service_idservice = $service_idservice;
    }
    
    public function create(){
        
        try {
            
            $dataSql = "insert into enterprise_has_service (`enterprise_identerprise`, `service_idservice`)";
            $dataSql .= " values ($this->enterprise_identerprise, $this->service_idservice);";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
            
            return true;
            
        } catch (Exception $exc) {
            
            echo $exc->getTraceAsString();
            return false;
            
        }
        
    }
    
    static public function delete($identerprise){
        try{
            $dataSql = "delete from enterprise_has_service where enterprise_identerprise = $identerprise;";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
        }
        catch(Exception $e){
            echo $e->getTraceAsString();
        }
    }
    
    static public function listAssocSub($identerprise){
        try {
            $dataSql = "select * from enterprise_has_service ehs inner join service s on ehs.service_idservice = s.idservice where ehs.enterprise_identerprise = $identerprise;";
            return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetchAll();        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}