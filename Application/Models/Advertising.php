<?php

/**
 * Description of Advertising
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

class Advertising {
    
    private $idadvertising;
    private $title;
    private $content;
    private $enterprise_identerprise;
    private $idadvertising_type;
    
    public function getIdadvertising() {
        return $this->idadvertising;
    }

    public function setIdadvertising($idadvertising) {
        $this->idadvertising = $idadvertising;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
    }
    public function getEnterprise_identerprise() {
        return $this->enterprise_identerprise;
    }

    public function setEnterprise_identerprise($enterprise_identerprise) {
        $this->enterprise_identerprise = $enterprise_identerprise;
    }
    
    public function getIdadvertising_type() {
        return $this->idadvertising_type;
    }

    public function setIdadvertising_type($idadvertising_type) {
        $this->idadvertising_type = $idadvertising_type;
    }
    
    function __construct($idadvertising, $title, $content, $enterprise_identerprise, $idadvertising_type) {
        $this->idadvertising = $idadvertising;
        $this->title = $title;
        $this->content = $content;
        $this->enterprise_identerprise = $enterprise_identerprise;
        $this->idadvertising_type = $idadvertising_type;
    }

    /**
     * 
     * @param int $identerprise Enterprises's ID
     * @return Array
     * @author Thiago Moreira <loganguns@gmail.com>
     */
    
    static public function search($id = null) {
        try {
            
            $dataSql = "select * from advertising";
            
            if($id == null){
                return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetchAll();
            }
            else{
                $dataSql .= " where `idadvertising` = $id";
                return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetch();
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public function create(){
        //try {
            $dataAdvertising = array(
                //'value'      => new Zend_Db_Expr('CURDATE()'),
                'title'                         => $this->title,
                'content'                       => $this->content,
                'type_advertising_idtype_advertising'                       => $this->idadvertising_type,
                'enterprise_identerprise'       => $this->enterprise_identerprise
            );
            
            Zend_Registry::get("DB")->insert('advertising', $dataAdvertising);
            
            //print_r($dataAdvertising);
            
            return true;
            
        //} catch (Exception $exc) {
        //    echo $exc->getTraceAsString();
        //}
    }
    
    public function edit(){
        try {
            $dataSql = "update advertising set ";
            $dataSql .= "`title` = '$this->title', ";
            $dataSql .= "`content` = '$this->content' ";
            $dataSql .= "where idadvertising = '$this->idadvertising'";
            
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }
}