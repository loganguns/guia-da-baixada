<?php

Zend_Loader::loadClass('User');
Zend_Loader::loadClass('Zend_Auth_Adapter_DbTable');

class Auth
{
    public static function login($email, $password)
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        //Inicia o adaptador Zend_Auth para banco de dados
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $authAdapter->setTableName('user')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password')
                    ->setCredentialTreatment('SHA1(?)');
        //Define os dados para processar o login
        $authAdapter->setIdentity($email)
                    ->setCredential($password);
        //Faz inner join dos dados do perfil no SELECT do Auth_Adapter
        $select = $authAdapter->getDbSelect();
        //$select->join( array('p' => 'perfil'), 'p.id = usuario.perfil_id', array('nome_perfil' => 'nome') );
        $select->where('status = ?', 1);
        //Efetua o login
        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($authAdapter);
        //Verifica se o login foi efetuado com sucesso
        if ( $result->isValid() ) {
            //Recupera o objeto do usuário, sem a senha
            $info = $authAdapter->getResultRowObject(null, 'password');
            
            if($info->user_type_iduser_type == '1'){ $role = 'user';}
            elseif($info->user_type_iduser_type == '4'){ $role = 'admin';}
            else{ $role = 'guest'; }
            
            $User = new User();
            $User->setIduser($info->iduser);
            $User->setEmail($info->email);
            $User->setName($info->name);
            //$User->setRoleId($role);
            $User->setEmail($info->email);
            $User->setUser_type_iduser_type($info->User_type_iduser_type);
            //$User->setUserInfo($info);
            $User->setStatus(true);
            
            $auth = new Zend_Session_Namespace('loginUser');
            $auth->login = true;
            $auth->iduser = $User->getIduser();
            $auth->name = $User->getName();
            //$auth->password = $User->getPassword();
            $auth->email = $User->getEmail();
            //$auth->userInfo = $User->getUserInfo();
            $auth->userType = $User->getUser_type_iduser_type();
            
//$storage = $auth->getStorage();
            //$storage->write($User);
            
            return true;
        }
        
        throw new Exception('Nome de usuário ou senha inválida');
    }
}