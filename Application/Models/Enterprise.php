<?php

/**
 * Description of Enterprise
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */
class Enterprise {

    private $identerprise;
    private $enterprise_name;
    private $description;
    private $franchise_idfranchise;
    private $iduser;
    private $idaddress;

    public function getIdenterprise() {
        return $this->identerprise;
    }

    public function setIdenterprise($identerprise) {
        $this->identerprise = $identerprise;
    }

    public function getEnterprise_name() {
        return $this->enterprise_name;
    }

    public function setEnterprise_name($enterprise_name) {
        $this->enterprise_name = $enterprise_name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getFranchise_idfranchise() {
        return $this->franchise_idfranchise;
    }

    public function setFranchise_idfranchise($franchise_idfranchise) {
        $this->franchise_idfranchise = $franchise_idfranchise;
    }

    public function getIduser() {
        return $this->iduser;
    }

    public function setIduser($iduser) {
        $this->iduser = $iduser;
    }
    
    public function getIdaddress() {
        return $this->idaddress;
    }

    public function setIdaddress($idaddress) {
        $this->idaddress = $idaddress;
    }
    
    function __construct($identerprise, $enterprise_name, $description, $franchise_idfranchise, $iduser, $idaddress) {
        $this->identerprise = $identerprise;
        $this->enterprise_name = $enterprise_name;
        $this->description = $description;
        $this->franchise_idfranchise = $franchise_idfranchise;
        $this->iduser = $iduser;
        $this->idaddress = $idaddress;
    }

    /**
     * 
     * @author Thiago Moreira <loganguns@gmail.com>
     */
    public function create(){
        //try {
            
            $dataEnterprise = array(
                'identerprise' => $this->identerprise,
                'name' => $this->enterprise_name,
                'description' => $this->description,
                'franchise_idfranchise' => $this->franchise_idfranchise,
                'address_idaddress' => $this->idaddress
            );
            Zend_Registry::get("DB")->insert('enterprise', $dataEnterprise);
            
            //print_r($dataEnterprise);
            
            $this->identerprise = Zend_Registry::get("DB")->lastInsertId();
            $dataUser = array(
                'user_iduser'               => $this->iduser,
                'enterprise_identerprise'   => $this->identerprise
            );
            Zend_Registry::get("DB")->insert('user_has_enterprise', $dataUser);
            
            return true;
            
//        } catch (Exception $exc) {
//            echo $exc->getTraceAsString();
//        }
    }

    /**
     * 
     * @param int $identerprise Enterprises's ID
     * @return Array
     * @author Thiago Moreira <loganguns@gmail.com>
     */
    static public function search($id = null) {
        try {
            
            $dataSql = "select * from enterprise e";
            
            if($id == null){
                return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetchAll();
            }
            else{
                $dataSql .= " left join address a on a.idaddress=e.address_idaddress where `identerprise` = $id";
                return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetch();
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        
//        foreach($list as $i){
//            $dataAdvertising = array(
//                //'value'      => new Zend_Db_Expr('CURDATE()'),
//                'title'                         => '',
//                'content'                       => '',
//                'type_advertising_idtype_advertising'                       => '10',
//                'enterprise_identerprise'       => $i['identerprise']
//            );
//            
//            Zend_Registry::get("DB")->insert('advertising', $dataAdvertising);
//        }
    }
    
    public function edit(){
        try {
            $dataSql = "update enterprise set ";
            $dataSql .= "`name` = '$this->enterprise_name', ";
            $dataSql .= "`description` = '$this->description', ";
            $dataSql .= "`franchise_idfranchise` = '$this->franchise_idfranchise' ";
            $dataSql .= "where identerprise = '$this->identerprise';";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
            return true;
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }
    
    public static function getContactInfo($enterpriseId){
        try {
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from(array("ehc" => "enterprise_has_contact"), '')
                    ->join(array("c" => "contact"), "ehc.contact_idcontact = c.idcontact", 'value')
                    ->join(array("ct" => "contact_type"), "ct.idcontact_type = c.contact_type_idcontact_type", 'type_name')
                    ->where('enterprise_identerprise = ?', $enterpriseId);
            $exec = $db->query($dataSql)->fetchAll();
            
            return $exec;
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}