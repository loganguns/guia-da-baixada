<?php

/**
 * Description of Search
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

class Announcement extends Zend_Db_Table_Abstract {
   
    private $idadvertising;
    private $content;
    private $title;
    private $enterprise_identerprise;
    private $status;
    private $addresses = Array();
    private $type_advertising_idtype_advertising;
    
    public function getIdadvertising() {
        return $this->idadvertising;
    }

    public function getContent() {
        return $this->content;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getEnterprise_identerprise() {
        return $this->enterprise_identerprise;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getAddresses() {
        return $this->addresses;
    }

    public function getType_advertising_idtype_advertising() {
        return $this->type_advertising_idtype_advertising;
    }

    public function setIdadvertising($idadvertising) {
        $this->idadvertising = $idadvertising;
    }

    public function setContent($content) {
        $this->content = $content;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setEnterprise_identerprise($enterprise_identerprise) {
        $this->enterprise_identerprise = $enterprise_identerprise;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setAddresses($addresses) {
        $this->addresses = $addresses;
    }

    public function setType_advertising_idtype_advertising($type_advertising_idtype_advertising) {
        $this->type_advertising_idtype_advertising = $type_advertising_idtype_advertising;
    }

        
     public static function search(array $term = null){ 
        try {
            
            $db = Zend_Registry::get('DB');
            $term['searchTerm'] = str_replace(' ','%', $term['searchTerm']);
            //$local = explode(' ', $local);
            $dataSql = "select * from advertising a 
                left join enterprise e on a.enterprise_identerprise = e.identerprise 
                
                left join enterprise_has_service ehs on ehs.enterprise_identerprise = e.identerprise
                left join service se on ehs.service_idservice = se.idservice";
            $dataSql .= " where e.name like '%".$term['searchTerm']."%'
                        or a.content like '%".$term['searchTerm']."%' 
                        or a.title like '%".$term['searchTerm']."%' 
                        or e.description like '%".$term['searchTerm']."%' 
                        or se.service_name like '%".$term['searchTerm']."%'
                        or se.description like '%".$term['searchTerm']."%' ";
            $dataSql .= " group by a.idadvertising
                limit 0,10";
            
            return $db->query($dataSql)->fetchAll();
            
        } catch (Exception $exc) {
            Zend_Debug::dump($exc);
        }
     }
     
    public static function searchMultiCidade($term = null, $local = null){ # Funçao pesquisa com varias cidades
        try {
            $db = Zend_Registry::get('DB');
            $term = explode(' ', $term);
            //$local = explode(' ', $local);
            $dataSql = "select * from advertising a 
                left join enterprise e on a.enterprise_identerprise = e.identerprise 
                left join address ad on e.address_idaddress = ad.idaddress
                left join district d on ad.district_iddistrict = d.iddistrict
                left join city c on ad.city_idcity = c.idcity
                left join state s on ad.state_idstate = s.idstate 
                left join enterprise_has_service ehs on ehs.enterprise_identerprise = e.identerprise
                left join service se on ehs.service_idservice = se.idservice";
            
            foreach ($term as $key => $arr) {
                if(strlen($arr) >= 3){
                    if($key == 0){$dataSql .= " where ";}else{$dataSql .= " or ";}
                       $dataSql .= "e.name like '%$arr%'
                        or a.content like '%$arr%' 
                        or a.title like '%$arr%' 
                        or e.description like '%$arr%' 
                        or se.service_name like '%$arr%'
                        or se.description like '%$arr%' ";
                }
            }
            $dataSql .= " and c.city_name = '$local'";
            $dataSql .= " group by a.idadvertising
                limit 0,10";
            
            $dataSqlCount = "select count('idadvertising') from advertising a
                left join enterprise e on a.enterprise_identerprise = e.identerprise
                left join address ad on e.address_idaddress = ad.idaddress
                left join district d on ad.district_iddistrict = d.iddistrict
                left join city c on ad.city_idcity = c.idcity
                left join state s on ad.state_idstate = s.idstate
                left join enterprise_has_service ehs on ehs.enterprise_identerprise = e.identerprise
                left join service se on ehs.service_idservice = se.idservice";
            
            foreach ($term as $key => $arr) {
                if(strlen($arr) >= 3){
                    if($key == 0){$dataSqlCount .= " where ";}else{$dataSqlCount .= " or ";}
                       $dataSqlCount .= "e.name like '%$arr%'
                        or a.content like '%$arr%'
                        or a.title like '%$arr%'
                        or e.description like '%$arr%'
                        or se.service_name like '%$arr%'
                        or se.description like '%$arr%' ";
                }
            }
            $dataSqlCount .= " and c.city_name = '$local'";
            $dataSqlCount .= " group by a.idadvertising";
            //print_r($db->query($dataSqlCount)->fetch());
            
//            echo $dataSql;
//            $dataSql = $db->select()
//                ->from(array('a' => 'advertising'), array ("*"))
//                ->join(array('e' => 'enterprise'), 'a.enterprise_identerprise = e.identerprise')
//                ->join(array('ad' => 'address'), 'e.address_idaddress = ad.idaddress')
//                ->where("e.name LIKE ?", '%'.$term.'%')
//                ->where("e.description LIKE ?", '%'.$term.'%')
//                ->where("a.title LIKE ?", '%'.$term.'%')
//                ->where("a.content LIKE ?", '%'.$term.'%')
//                ->group("a.idadvertising");
//                print_r($dataSql);
            
            return $db->query($dataSql)->fetchAll();
        
        } catch (Exception $exc) {
            Zend_Debug::dump($exc);
        }
    }
}