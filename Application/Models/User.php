<?php

/**
 * Description of User
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */
Zend_Loader::loadClass('Zend_Db_Select');
Zend_Loader::loadClass('Zend_Db_Statement_Mysqli');

class User {

    private $iduser;
    private $email;
    private $password;
    private $name;
    private $status;
    private $deleted;
    private $user_type_iduser_type;
    private $address_idaddress;
    private $activateCode;
    private $userInfo;

    public function getIduser() {
        return $this->iduser;
    }

    public function setIduser($iduser) {
        $this->iduser = $iduser;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setDeleted($deleted) {
        $this->delete = $deleted;
    }

    public function getUser_type_iduser_type() {
        return $this->user_type_iduser_type;
    }

    public function setUser_type_iduser_type($user_type_iduser_type) {
        $this->user_type_iduser_type = $user_type_iduser_type;
    }

    public function getAddress_idaddress() {
        return $this->address_idaddress;
    }

    public function setAddress_idaddress($address_idaddress) {
        $this->address_idaddress = $address_idaddress;
    }
    public function getActivateCode() {
        return $this->activateCode;
    }

    public function setActivateCode($activateCode) {
        $this->activateCode = $activateCode;
    }
    public function getUserInfo() {
        return $this->userInfo;
    }

    public function setUserInfo($userInfo) {
        $this->userInfo = $userInfo;
    }

    function __construct($iduser, $email, $password, $name, $status, $deleted, $user_type_iduser_type, $address_idaddress, $activateCode) {
        $this->iduser = $iduser;
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->status = $status;
        $this->deleted = $deleted;
        $this->user_type_iduser_type = $user_type_iduser_type;
        $this->address_idaddress = $address_idaddress;
        $this->activateCode = $activateCode;
    }

    function create() {
        try {
            $data = array(
                "email" => $this->email,
                "password" => sha1($this->password),
                "name" => $this->name,
                "status" => $this->status,
                "deleted" => $this->deleted,
                "user_type_iduser_type" => $this->user_type_iduser_type,
                "activateCode" => $this->activateCode);

            Zend_Registry::get("DB")->insert("user", $data);
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    function edit() {
        try {
            $new_email = $this->email;
            $new_password = $this->password;
            $new_name = $this->name;
            $new_status = $this->status;
            $new_delete = $this->delete;

            $dataSql = "update user set ('email', 'password', 'name', 'status', 'deleted')";
            $dataSql .= "values ('$new_email', '$new_password', '$new_name', '$new_status', '$new_delete');";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
        } catch (Exception $e) {
            echo $e;
        }
    }

    /**
     * Authentication Method
     * @author Thiago Moreira
     * @param string $email
     * @param string $password
     *
     */
    public function login() {
        $db = Zend_Registry::get('DB');
        //$dataSql = "select * from user where email = '$email' and password = sha('$password') and status = 1 and `delete` = 0";
        //$dataSql = "select * from user where email = ? and password = sha(?) and status = 1 and `delete` = 0";
        //$exec = $db->query($dataSql)->fetch();
        //$db->prepare($dataSql);
        //$db->bindValue(1, $email);
        //$db->bindValue(2, $password);
        //$exec = $db->query($dataSql)->fetch();
        //$stmt = new Zend_Db_Statement_Mysqli($db, $dataSql);
        //$stmt->execute(array($email, $password));
        //$stmt->fetch($dataSql, array($email, $password));
        //$select = new Zend_Db_Select($db);
        $dataSql = $db->select()
                ->from("user")
                ->where('email = ?', $this->email)
                ->where('password = sha(?)', $this->password)
                ->where('deleted = ?', 0)
                ->where('status = ?', 1);
        $exec = $db->query($dataSql)->fetch();

        if ($exec) {
            $this->setIduser($exec['iduser']);
            $this->setName($exec['name']);
            $this->setPassword($exec['password']);
            $this->setEmail($exec['email']);
            $this->setUserInfo($exec);
            $this->setStatus(true);

            return true;
        } else {
            $this->setStatus(false);

            return false;
        }
    }

    /**
     * Logoff Method
     * @author Thiago Moreira
     *
     */
    public static function logoff() {
        $login_status = new Zend_Session_Namespace('login');
        unset($login_status->logado);
    }

    /**
     * Is unique email?
     *
     * Check if $email is unique email in database
     *
     * @param  string $name
     * @return boolean
     */
    public static function isUniqueEmail($email) {

        $db = Zend_Registry::get('DB');
        $dataSql = $db->select()
                ->from("user")
                ->where("email = ?", $email);
        $exec = $db->getConnection()->query($dataSql)->fetch();

        //echo '<pre>userModel ';
        //print_r ($exec);
        //echo '</pre>';

        if ($exec) {
            //echo "ja cadastrado | return False";
            return False;
        } else {
            //echo "ainda nao cadastrado | retun True";
            return True;
        }
    }

    public static function delete($iduser) {
        try {
            $dataSql = "update user set `deleted` = 1 where iduser = $iduser";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
            return true;
        } catch (Exception $e) {
            echo $e->getTraceAsString();
        }
    }

    static public function randomPassword($lenght) {
        $alphabet = "abcdefghijklmnopqrstuwxyz";
        $alphabet .= "ABCDEFGHIJKLMNOPQRSTUWXYZ";
        $alphabet .= "0123456789";
        $alphabet .= "!@#$%*()_+-=";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $lenght; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    static public function changePass($email, $newpass) {

        $dataSql = "update `user` set `password` = sha('$newpass') where email = '$email'";
        Zend_Registry::get("DB")->getConnection()->query($dataSql);
    }
    
    static public function activate($email, $activationCode){
        try {
            
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from(array('user'))
                    ->where('activateCode = ?', $activationCode);
            $exec = $db->getConnection()->query($dataSql)->fetch();
            if($exec){
                $data = array(
                    'status'      => '1',
                    'activateCode' => ''
                );
                $where['email = ?'] = $email;
                $where['activateCode = ?'] = $activationCode;
                $where['status = ?'] = '0';
                $update = $db->update('user', $data, $where);
                
                if($update){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public static function getList(){
        
        try {
            $db = Zend_Registry::get('DB');
            
            $data = $db->select()
                    ->from('user');
            
            $exec = $db->getConnection()->query($data)->fetchAll();
            
            return $exec;
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            Zend_Debug::dump($exc);
        }
    }
}