<?php

/**
 * Class of Address Manipulation
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */
class Address {

    private $idaddress;
    private $address;
    private $number;
    private $complement;
    private $status;
    private $latitute;
    private $longitude;
    private $type_address_idtype_address;
    private $state_idstate;
    private $city_idcity;
    private $district_iddistrict;
    private $country_idcountry;

    public function getIdaddress() {
        return $this->idaddress;
    }

    public function setIdaddress($idaddress) {
        $this->idaddress = $idaddress;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setNumber($number) {
        $this->number = $number;
    }

    public function getComplement() {
        return $this->complement;
    }

    public function setComplement($complement) {
        $this->complement = $complement;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getType_address_idtype_address() {
        return $this->type_address_idtype_address;
    }

    public function setType_address_idtype_address($type_address_idtype_address) {
        $this->type_address_idtype_address = $type_address_idtype_address;
    }

    public function getState_idstate() {
        return $this->state_idstate;
    }

    public function setState_idstate($state_idstate) {
        $this->state_idstate = $state_idstate;
    }

    public function getCity_idcity() {
        return $this->city_idcity;
    }

    public function setCity_idcity($city_idcity) {
        $this->city_idcity = $city_idcity;
    }

    public function getDistrict_iddistrict() {
        return $this->district_iddistrict;
    }

    public function setDistrict_iddistrict($district_iddistrict) {
        $this->district_iddistrict = $district_iddistrict;
    }

    public function getCountry_idcountry() {
        return $this->country_idcountry;
    }

    public function setCountry_idcountry($country_idcountry) {
        $this->country_idcountry = $country_idcountry;
    }
    public function getLatitute() {
        return $this->latitute;
    }

    public function setLatitute($latitute) {
        $this->latitute = $latitute;
    }

    public function getLongitude() {
        return $this->longitude;
    }

    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function __construct($address, $number, $complement, $status, $latitute, $longitude, $type_address_idtype_address, $state_idstate, $city_idcity, $district_iddistrict, $country_idcountry, $idaddress = null) {
        $this->idaddress = $idaddress;
        $this->address = $address;
        $this->number = $number;
        $this->complement = $complement;
        $this->status = $status;
        $this->latitute = $latitute;
        $this->longitude = $longitude;
        $this->type_address_idtype_address = $type_address_idtype_address;
        $this->state_idstate = $state_idstate;
        $this->city_idcity = $city_idcity;
        $this->district_iddistrict = $district_iddistrict;
        $this->country_idcountry = $country_idcountry;
    }

    
    function create(){
        
        try{
            $data = array(  'address'                       => $this->getAddress(),
                            'number'                        => $this->getNumber(),
                            'complement'                    => $this->getComplement(),
                            'status'                        => $this->getStatus(),
                            'type_address_idtype_address'   => $this->getType_address_idtype_address(),
                            'state_idstate'                 => $this->getState_idstate(),
                            'city_idcity'                   => $this->getCity_idcity(),
                            'district_iddistrict'           => $this->getDistrict_iddistrict(),
                            'country_idcountry'             => $this->getCountry_idcountry()
            );
            Zend_Registry::get("DB")->insert('address', $data);
            $this->idaddress = Zend_Registry::get("DB")->lastInsertId();
            return true;
        }
        catch(Exception $e){
            echo $e->getTraceAsString();
        }
    }
    
    public function edit(){
        try {
            $address = $this->getAddress();
            $number = $this->getNumber();
            $complement = $this->getComplement();
            $status = $this->getStatus();
            $type_address_idtype_address = $this->getType_address_idtype_address();
            $state_idstate = $this->getState_idstate();
            $city_idcity = $this->getCity_idcity();
            $district_iddistrict = $this->getDistrict_iddistrict();
            $country_idcountry = $this->getCountry_idcountry();
            
            $dataSql = "update address set ";
            $dataSql .= "`address` = '$address', ";
            $dataSql .= "`number` = '$number', ";
            $dataSql .= "`complement` = '$complement', ";
            $dataSql .= "`status` = $status, " ;
            $dataSql .= "`type_address_idtype_address` = $type_address_idtype_address, ";
            $dataSql .= "`state_idstate` = $state_idstate, ";
            $dataSql .= "`city_idcity` = $city_idcity, ";
            $dataSql .= "`district_iddistrict` = $district_iddistrict, ";
            $dataSql .= "`country_idcountry` = $country_idcountry ";
            $dataSql .= "where idaddress = $this->idaddress;";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
            return true;
        
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }
    
    static public function listState(){
        try{
            $dataSql = "select * from state";
            return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetchAll();
        }
        catch(Exception $e){
            echo $e->getTraceAsString();
        }
    }
    
    static public function listCity($idstate){
        try{
            $dataSql = "select * from city where state_idstate = $idstate";
            return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetchAll();
        }
        catch(Exception $e){
            echo $e->getTraceAsString();
        }
        
    }
}