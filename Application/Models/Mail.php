<?php

Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');
Zend_Loader::loadClass('Zend_Mail');

/**
 * Description of Mail
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

class Mail {
    private $to;
    private $body;
    private $header;
    private $nameTo;
    private $subject;
    
    public function getTo() {
        return $this->to;
    }

    public function setTo($to) {
        $this->to = $to;
    }

    public function getBody() {
        return $this->body;
    }

    public function setBody($body) {
        $this->body = $body;
    }

    public function getHeader() {
        return $this->header;
    }

    public function setHeader($header) {
        $this->header = $header;
    }
    
    public function getNameTo() {
        return $this->nameTo;
    }

    public function setNameTo($nameTo) {
        $this->nameTo = $nameTo;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }
    function __construct($to, $body, $header, $nameTo, $subject) {
        $this->to = $to;
        $this->body = $body;
        $this->header = $header;
        $this->nameTo = $nameTo;
        $this->subject = $subject;
    }

    public function send(){
        try{
            $config = array (
            'auth' =>         "login",
            'username' =>     "noreply@catalogodaregiao.com.br",
            'password' =>     "catalogo3251XP",
            //'ssl' =>          "ssl",
            'port' =>         "587"
            );
            $mailTransport = new Zend_Mail_Transport_Smtp("smtp.catalogodaregiao.com.br",$config);

            $mail = new Zend_Mail('UTF-8');
            $mail->setBodyHtml($this->body,'UTF-8');
            $mail->addTo($this->to,$this->nameTo);
            $mail->setFrom("noreply@catalogodaregiao.com.br","Guia da Baixada");
            $mail->setReplyTo("noreply@catalogodaregiao.com.br","Guia da Baixada");
            $mail->setReturnPath("noreply@catalogodaregiao.com.br","Guia da Baixada"); #muito importante na locaweb, informar um email válido do seu dominio
            //$mail->addBcc($emailBcc);
            $mail->setSubject($this->subject);
            $mail->send($mailTransport);
            return true;
        }catch(Exception $e){
            Zend_Debug::dump($e);
            return false;
        }
    }
}