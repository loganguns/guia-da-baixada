<?php

/**
 * Description of Search
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

class Search {
    
    private $idadvertising;
    private $content;
    private $title;
    private $enterprise_identerprise;
    private $identerprise;
    private $name;
    private $description;
    private $franchise_idfranchise;
    private $address_idaddress;
    private $idaddress;
    private $address;
    private $number;
    private $complement;
    private $status;
    private $latitude;
    private $longitude;
    private $type_address_idtype_address;
    private $state_idstate;
    private $city_idcity;
    private $country_idcountry;
    private $district_iddistrict;
    
    public function getIdadvertising() {
        return $this->idadvertising;
    }

    public function setIdadvertising($idadvertising) {
        $this->idadvertising = $idadvertising;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getEnterprise_identerprise() {
        return $this->enterprise_identerprise;
    }

    public function setEnterprise_identerprise($enterprise_identerprise) {
        $this->enterprise_identerprise = $enterprise_identerprise;
    }

    public function getIdenterprise() {
        return $this->identerprise;
    }

    public function setIdenterprise($identerprise) {
        $this->identerprise = $identerprise;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getFranchise_idfranchise() {
        return $this->franchise_idfranchise;
    }

    public function setFranchise_idfranchise($franchise_idfranchise) {
        $this->franchise_idfranchise = $franchise_idfranchise;
    }

    public function getAddress_idaddress() {
        return $this->address_idaddress;
    }

    public function setAddress_idaddress($address_idaddress) {
        $this->address_idaddress = $address_idaddress;
    }

    public function getIdaddress() {
        return $this->idaddress;
    }

    public function setIdaddress($idaddress) {
        $this->idaddress = $idaddress;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setNumber($number) {
        $this->number = $number;
    }

    public function getComplement() {
        return $this->complement;
    }

    public function setComplement($complement) {
        $this->complement = $complement;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getLatitude() {
        return $this->latitude;
    }

    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    public function getLongitude() {
        return $this->longitude;
    }

    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    public function getType_address_idtype_address() {
        return $this->type_address_idtype_address;
    }

    public function setType_address_idtype_address($type_address_idtype_address) {
        $this->type_address_idtype_address = $type_address_idtype_address;
    }

    public function getState_idstate() {
        return $this->state_idstate;
    }

    public function setState_idstate($state_idstate) {
        $this->state_idstate = $state_idstate;
    }

    public function getCity_idcity() {
        return $this->city_idcity;
    }

    public function setCity_idcity($city_idcity) {
        $this->city_idcity = $city_idcity;
    }

    public function getCountry_idcountry() {
        return $this->country_idcountry;
    }

    public function setCountry_idcountry($country_idcountry) {
        $this->country_idcountry = $country_idcountry;
    }

    public function getDistrict_iddistrict() {
        return $this->district_iddistrict;
    }

    public function setDistrict_iddistrict($district_iddistrict) {
        $this->district_iddistrict = $district_iddistrict;
    }

    function __construct($idadvertising, $content, $title, $enterprise_identerprise, $identerprise, $name, $description, $franchise_idfranchise, $address_idaddress, $idaddress, $address, $number, $complement, $status, $latitude, $longitude, $type_address_idtype_address, $state_idstate, $city_idcity, $country_idcountry, $district_iddistrict) {
        $this->idadvertising = $idadvertising;
        $this->content = $content;
        $this->title = $title;
        $this->enterprise_identerprise = $enterprise_identerprise;
        $this->identerprise = $identerprise;
        $this->name = $name;
        $this->description = $description;
        $this->franchise_idfranchise = $franchise_idfranchise;
        $this->address_idaddress = $address_idaddress;
        $this->idaddress = $idaddress;
        $this->address = $address;
        $this->number = $number;
        $this->complement = $complement;
        $this->status = $status;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->type_address_idtype_address = $type_address_idtype_address;
        $this->state_idstate = $state_idstate;
        $this->city_idcity = $city_idcity;
        $this->country_idcountry = $country_idcountry;
        $this->district_iddistrict = $district_iddistrict;
    }


}