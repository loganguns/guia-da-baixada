<?php

/**
 * Description of Service
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */

class Service {
    
    private $idservice;
    private $category_idcategory;
    private $service_name;
    private $description;
    
    public function getIdservice() {
        return $this->idservice;
    }

    public function setIdservice($idservice) {
        $this->idservice = $idservice;
    }

    public function getCategory_idcategory() {
        return $this->category_idcategory;
    }

    public function setCategory_idcategory($category_idcategory) {
        $this->category_idcategory = $category_idcategory;
    }

    public function getService_name() {
        return $this->service_name;
    }

    public function setService_name($service_name) {
        $this->service_name = $service_name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    function __construct($category_idcategory, $service_name, $description, $idservice = null) {
        $this->idservice = $idservice;
        $this->category_idcategory = $category_idcategory;
        $this->service_name = $service_name;
        $this->description = $description;
    }

    public function create(){
        try {
            
            $dataSql = "insert into service (`category_idcategory`,`service_name`,`description`)";
            $dataSql .= " values ('$this->category_idcategory','$this->service_name','$this->description');";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function edit(){
        try {
            $dataSql = "update service set ";
            $dataSql .= "`idservice` = '$this->idservice', ";
            $dataSql .= "`description` = '$this->description', ";
            $dataSql .= "`service_name` = '$this->service_name', ";
            $dataSql .= "`category_idcategory` = '$this->category_idcategory' ";
            $dataSql .= "where idservice = '$this->idservice';";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
            return true;
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }
    
    /**
     * 
     * @param int $idcategory Category's ID
     * @return Array
     */
    static public function search($id) {
        try {
            $db = Zend_Registry::get("DB");
            
            $dataSql = $db->select()
                ->from("service")
                ->where('idservice = ?', $id);
            return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetch();
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
     public static function _list(){
        try{
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from('service');
            
            $exec = $db->query($dataSql)->fetchAll();
        
            return $exec;
        } catch (Exception $exec){
            echo $exec->getTraceAsString();
        }
    }
    
    public static function searchByName($name){
        try{
                //$dataSql = "SELECT idservice as id, service_name from service WHERE `deleted` = 0 and service_name LIKE '%$q%' ORDER BY service_name DESC LIMIT 10";
            $db = Zend_Registry::get('DB');    
            $dataSql = $db->select()
                    ->from("service", array(
                        'id' => 'idservice',
                        'service_name'
                    ))
                    ->where("deleted = ?", 0)
                    ->where("service_name LIKE ?", '%'.$name.'%')
                    ->order("service_name DESC")
                    ->limit(10);
                return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetchAll();
          
        } catch(Exception $exec){
            echo $exec->getTraceAsString();    
                    
        }
    }
    
    public static function searchByCategoryId($categoryid){
        try{
                //$dataSql = "SELECT idservice as id, service_name from service WHERE `deleted` = 0 and service_name LIKE '%$q%' ORDER BY service_name DESC LIMIT 10";
            $db = Zend_Registry::get('DB');    
            $dataSql = $db->select()
                    ->from("service", array(
                        'id' => 'idservice',
                        'name' => 'service_name'
                    ))
                    ->where("deleted = ?", 0)
                    ->where("category_idcategory = ?", $categoryid)
                    ->order("service_name DESC")
                    ->limit(10);
                return Zend_Registry::get("DB")->getConnection()->query($dataSql)->fetchAll();
          
        } catch(Exception $exec){
            echo $exec->getTraceAsString();    
                    
        }
    }
}