<?php

/**
 * Description of Category
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */
class Category {

    private $idcategory;
    private $name;
    private $description;

    public function getIdcategory() {
        return $this->idcategory;
    }

    public function setIdcategory($idcategory) {
        $this->idcategory = $idcategory;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
    function __construct( $name, $description, $idcategory = null) {
        $this->idcategory = $idcategory;
        $this->name = $name;
        $this->description = $description;
    }

    public function create() {
        try {
            $dataSql = "insert into category (`category_name`,`description`) ";
            $dataSql .= "values ('$this->name','$this->description')";

            //$db = Zend_Registry::get("DB");
            //$stmt = new Zend_Db_Statement_Mysqli($db, $dataSql);
            //$stmt->execute(array($this->name));

            Zend_Registry::get("DB")->getConnection()->query($dataSql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function edit(){
        try {
            $dataSql = "update category set ";
            $dataSql .= "`idcategory` = '$this->idcategory', ";
            $dataSql .= "`description` = '$this->description', ";
            $dataSql .= "`category_name` = '$this->name' ";
            $dataSql .= "where idcategory = '$this->idcategory';";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
            return true;
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }
    
    /**
     * 
     * @param int $idcategory Category's ID
     * @return Array
     */
    static public function search($categoryid) {
        try {
            $db = Zend_Registry::get('DB');
            $dataSql = $db->select()
                    ->from("category")
                    ->where('idcategory = ?', $categoryid)
                    ->order("category_name");
            
            return $db->getConnection()->query($dataSql)->fetch();
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public static function _list(){
        try {
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from('category');
            $exec = $db->query($dataSql)->fetchAll();
            
            return $exec;
        } catch (Exception $exec){
            echo $exec->getTraceAsString();
        }
    }
    
    public static function _listDestaque(){
        try {
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from(array("c" => "category"))
                    //->join(array("s" => "service"), "s.category_idcategory = c.idcategory")
                    ->where('destaque = ?', '1');
            
            $exec = $db->query($dataSql)->fetchAll();
            
            return $exec;
        } catch (Exception $exec){
            echo $exec->getTraceAsString();
        }
    }
}