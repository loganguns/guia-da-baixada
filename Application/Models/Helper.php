<?php

/**
 * Description of Helper
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */
class Helper {
    
    public static function getContactInfo($enterpriseId){
        try {
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from(array("ehc" => "enterprise_has_contact"), '')
                    ->join(array("c" => "contact"), "ehc.contact_idcontact = c.idcontact", 'value')
                    ->join(array("ct" => "contact_type"), "ct.idcontact_type = c.contact_type_idcontact_type", 'type_name')
                    ->where('enterprise_identerprise = ?', $enterpriseId);
            $exec = $db->query($dataSql)->fetchAll();
            
            return $exec;
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public static function getCategoryList(){
        try {
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from('category');
            $exec = $db->query($dataSql)->fetchAll();
            
            return $exec;
        } catch (Exception $exec){
            echo $exec->getTraceAsString();
        }
    }
    
    public static function getServiceList($idcategory){
        try{
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from('service')
                    ->where('category_idcategory = ?', $idcategory);
            
            $exec = $db->query($dataSql)->fetchAll();
        
            return $exec;
        } catch (Exception $exec){
            echo $exec->getTraceAsString();
        }
    }
    
    public static function getStateList(){
        try{
            $db = Zend_Registry::get('DB');
            
            $dataSql = $db->select()
                    ->from('state');
            
            $exec = $db->query($dataSql)->fetchAll();
            
            return $exec;
            
        } catch (Exception $exec) {
            echo $exec->getTraceAsString();
        }
    }
    
    public static function setGeolocationInfo($latlng){
        try{
            $jsonurl ="http://maps.googleapis.com/maps/api/geocode/json?latlng=$latlng&sensor=false";

            $json = file_get_contents($jsonurl,0,null,null);
            $json_output = Zend_Json::decode($json);

            $address = $json_output['results'][0]['formatted_address'];
            $exploded_address = explode(',', $address);
            foreach($exploded_address as $exploded){
                if(preg_match('/[a-zA-Z] \- [a-zA-Z]/', $exploded)){
                    $exploded2 = explode(' - ', $exploded);
                        
                        if( @$_COOKIE['city'] != trim($exploded2[0])){
                            
                                setcookie('city', trim($exploded2[0]), time() + 3600, '/');
                                setcookie('state', trim($exploded2[1]), time() + 3600, '/');
                                
                                return $status = 2; // Changed
                        }
                }
            }
            
            setcookie('latlng', trim($latlng), time() + 3600, '/');
            
            return $status = 1; // Not Changed
            
        } catch (Exception $exec){
            echo $exec->getTraceAsString();
        }
    }
}