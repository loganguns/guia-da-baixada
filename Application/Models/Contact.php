<?php

/**
 * Description of Contact
 *
 * @author Thiago Moreira <loganguns@gmail.com>
 */
class Contact {
    
    private $idcontact;
    private $name;
    private $status;
    private $value;
    private $contact_type_idcontact_type;
    private $identerprise;
    
    public function getIdcontact() {
        return $this->idcontact;
    }

    public function setIdcontact($idcontact) {
        $this->idcontact = $idcontact;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function getContact_type_idcontact_type() {
        return $this->contact_type_idcontact_type;
    }

    public function setContact_type_idcontact_type($contact_type_idcontact_type) {
        $this->contact_type_idcontact_type = $contact_type_idcontact_type;
    }

    public function getIdenterprise() {
        return $this->identerprise;
    }

    public function setIdenterprise($identerprise) {
        $this->identerprise = $identerprise;
    }
    
    function __construct($idcontact, $name, $status, $value, $contact_type_idcontact_type, $identerprise) {
        $this->idcontact = $idcontact;
        $this->name = $name;
        $this->status = $status;
        $this->value = $value;
        $this->contact_type_idcontact_type = $contact_type_idcontact_type;
        $this->identerprise = $identerprise;
    }

        public function create(){
        try {
            $data = array(
                //'value'      => new Zend_Db_Expr('CURDATE()'),
                'value'                         => $this->value,
                'contact_name'                  => $this->name,
                'status'                        => $this->status,
                'contact_type_idcontact_type'   => $this->contact_type_idcontact_type
            );
            //print_r($data);
            
            Zend_Registry::get("DB")->insert('contact', $data);
            
            $this->idcontact = Zend_Registry::get("DB")->lastInsertId();
            $dataEnterprise = array(
                'enterprise_identerprise'   => $this->identerprise,
                'contact_idcontact'         => $this->idcontact
            );
            Zend_Registry::get("DB")->insert('enterprise_has_contact', $dataEnterprise);
            
            return true;
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    } 
    
    static public function listType(){
        try {
            $db = Zend_Registry::get("DB");
            $dataSql = $db->select()
                ->from("contact_type");
        return $db->query($dataSql)->fetchAll();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    static public function assocContact(){
        try{
            $identerprise = "select identerprise from enterprise order by identerprise DESC limit 1;";
            $identerprise = Zend_Registry::get("DB")->getConnection()->query($identerprise)->fetch();
            
            $idcontact = "select idcontact from contact order by idcontact DESC limit 1;";
            $idcontact = Zend_Registry::get("DB")->getConnection()->query($idcontact)->fetch();
            
            $dataSql = "insert into enterprise_has_contact (enterprise_identerprise, contact_idcontact) values ($identerprise[0], $idcontact[0])";
            Zend_Registry::get("DB")->getConnection()->query($dataSql);
        }
        catch(Exception $e){
            echo $e->getTraceAsString();
        }
    }
}